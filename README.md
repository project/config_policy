# Config Policy

Config Policy is a module to validate and automatically fix your configuration.
You can create policy that defines how your configuration should look.

## Overview

When you create a policy you add rules to it. Rules can support validation and fixing of configuration.
A rule can even alter forms to prevent configuration from not matching your policy.

Config Policy comes with some basic rules to get started. You can easily add more rules to Config Policy, because rules are just plugins that you can add.

## UI
The Config Policy module can be managed in the Drupal UI. There is no need to go edit configuration files.

Features accessible from the UI:
- Manage policy
- Configure rules
- Validate & fix configuration

## Drush command
Validate and fix configuration in your CI by using the Drush command.

Use `drush config-policy:validate` to validate your configuration.

Or use the short form: `drush cpv`.

Add the `--fix` flag to also fix your configuration if it does not match your policy. 
Want to fix configuration without confirmation? Add the `-y` flag.
