<?php

namespace Drupal\config_policy\Form\Policy;

use Drupal\config_policy\Entity\ConfigPolicyInterface;
use Drupal\config_policy\Policy\ConfigPolicyServiceInterface;
use Drupal\config_policy\Result\FixResultInterface;
use Drupal\config_policy\Result\ResultItem\ResultItemInterface;
use Drupal\config_policy\Result\ValidationResultInterface;
use Drupal\config_policy\Rule\ConfigRuleInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to delete a config policy.
 */
class ConfigPolicyValidateForm extends FormBase implements ContainerInjectionInterface {

  const STEP_CONFIRM = 'confirm';
  const STEP_RESULT = 'result';

  protected ConfigPolicyServiceInterface $configPolicyService;
  protected ?ConfigPolicyInterface $configPolicy;
  protected ?ConfigRuleInterface $configRule;
  protected ?ValidationResultInterface $validationResult = NULL;
  protected ?FixResultInterface $fixResult = NULL;

  public function __construct(ConfigPolicyServiceInterface $config_policy_interface) {
    $this->configPolicyService = $config_policy_interface;
  }

  public static function create(ContainerInterface $container): self {
    return new self($container->get('config_policy.service'));
  }

  public function getFormId(): string {
    return 'config_policy_validate_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, ConfigPolicyInterface $config_policy = NULL, $config_rule = NULL) {
    $this->loadParameters($config_policy, $config_rule);
    $step = $this->prepareStep($form_state);
    switch ($step) {
      case self::STEP_CONFIRM:
      default:
        return $this->buildConfirmForm($form, $form_state);

      case self::STEP_RESULT:
        return $this->buildResultForm($form);
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->submitResultForm($form, $form_state);
  }

  public function submitConfirmForm(array $form, FormStateInterface $form_state) {
    $prefix = $form_state->getValue('prefix') ?? '';
    $fix = $form_state->getValue('fix') ?? 0;

    try {
      $config_files = $this->configPolicyService->getConfigFiles(FALSE, $prefix);
    }
    catch (Exception $exception) {
      $config_files = [];
    }

    $this->validationResult = $this->configPolicyService->validate($config_files, FALSE, FALSE, $this->configPolicy, $this->configRule);
    if ($fix) {
      $this->fixResult = $this->configPolicyService->fix($config_files, FALSE, FALSE, $this->configPolicy, $this->configRule);
    }

    $this->setStep(self::STEP_RESULT, $form_state);
  }

  public function submitResultForm(array $form, FormStateInterface $form_state) {
    $form_state->setRedirectUrl($this->getRedirectUrl());
  }

  private function loadParameters(?ConfigPolicyInterface $config_policy, ?string $config_rule_id): void {
    $this->configPolicy = $config_policy;
    $this->configRule = $config_policy && $config_rule_id ? $this->configPolicy->getRule($config_rule_id) : NULL;
  }

  public function submitFormGotoStep(array $form, FormStateInterface $form_state): void {
    $this->setStep($form_state->getTriggeringElement()['#step'], $form_state);
  }

  private function prepareStep(FormStateInterface $form_state): string {
    $step = $this->getStep($form_state);
    $form_state->set('step', $step);

    return $step;
  }

  private function setStep(string $step, FormStateInterface $form_state): void {
    $form_state->set('step', $step);
    $form_state->setRebuild();
  }

  private function getStep(FormStateInterface $form_state): string {
    $step = $form_state->get('step');

    if ($step) {
      return $step;
    }
    else {
      return self::STEP_CONFIRM;
    }
  }

  private function buildConfirmForm(array $form, FormStateInterface $form_state): array {
    $form['#title'] = $this->t('Validate Config Policy');

    $form['#attributes']['class'][] = 'confirmation';
    $form['description'] = ['#markup' => $this->getValidationDescription()];
    $form['prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Configuration prefix'),
      '#maxlength' => 255,
      '#default_value' => $form_state->getValue('prefix') ?? '',
      '#description' => $this->t("Only validate configuration that starts with this prefix."),
    ];
    $form['fix'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically try to fix invalid configuration'),
      '#default_value' => $form_state->getValue('fix') ?? 0,
    ];
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['validate'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#submit' => ['::submitConfirmForm'],
      '#value' => $this->t('Validate'),
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $this->getRedirectUrl(),
      '#attributes' => ['class' => ['button']],
    ];

    return $form;
  }

  private function buildResultForm(array $form): array {
    $form['validate'] = [
      '#type' => 'container',
    ];
    $this->buildValidationResult($form['validate']);

    if (!$this->fixResult) {
      $form['fix_result_title'] = ['#markup' => '<br>' . $this->t('Nothing was fixed, as the fix option was not enabled.')];
    }
    else {
      $form['fix'] = [
        '#type' => 'container',
      ];
      $this->buildFixResult($form['fix']);
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['finish'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#submit' => ['::submitResultForm'],
      '#value' => $this->t('Finish'),
    ];
    $form['actions']['back'] = [
      '#type' => 'submit',
      '#limit_validation_errors' => [],
      '#submit' => ['::submitFormGotoStep'],
      '#step' => self::STEP_CONFIRM,
      '#value' => $this->t('Back'),
    ];
    return $form;
  }

  private function buildValidationResult(array &$form): void {
    $result_per_config = $this->validationResult->groupResultsByConfig(TRUE);
    $form['validation_result_title'] = ['#markup' => '<strong>' . $this->t("Validation results (@count invalid)", ['@count' => count($result_per_config)]) . '</strong><br>'];
    foreach ($result_per_config as $config_name => $items) {
      $form['validation_result'][$config_name] = [
        '#type' => 'details',
        '#title' => $this->formatPlural(count($items), "%config (@count issue)", "%config (@count issues)", ['%config' => $config_name]),
      ];
      $form['validation_result'][$config_name]['results'] = [
        '#type' => 'table',
        '#header' => [
          $this->t('Type'),
          $this->t('Description'),
          $this->t('Rule'),
        ],
        '#attributes' => [
          'id' => $config_name . '_validation_result',
        ],
        '#empty' => $this->t("No errors or warnings found while validating '%config'", ['%config' => $config_name]),
        '#weight' => 5,
      ];
      $this->addResultRows($form['validation_result'][$config_name]['results'], $items);
    }
    if (empty($result_per_config)) {
      $form['validation_result_empty'] = ['#markup' => $this->t('No issues found.')];
    }
  }

  private function buildFixResult(array &$form): void {
    $result_per_config = $this->fixResult->groupResultsByConfig(FALSE);
    $fixed_count = count($this->fixResult->get('ok'));
    $form['fix_result_title'] = ['#markup' => '<br><strong>' . $this->t("Fix results (@count fixed)", ['@count' => $fixed_count]) . '</strong><br>'];
    foreach ($result_per_config as $config_name => $items) {
      $form['fix_result'][$config_name] = [
        '#type' => 'details',
        '#title' => $this->formatPlural($fixed_count, "%config (@count fix)", "%config (@count fixes)", ['%config' => $config_name]),
      ];
      $form['fix_result'][$config_name]['results'] = [
        '#type' => 'table',
        '#header' => [
          $this->t('Type'),
          $this->t('Description'),
          $this->t('Rule'),
        ],
        '#attributes' => [
          'id' => $config_name . '_fix_result',
        ],
        '#empty' => $this->t("No errors or warnings found to fix for '%config'", ['%config' => $config_name]),
        '#weight' => 5,
      ];
      $this->addResultRows($form['fix_result'][$config_name]['results'], $items);
    }
    if (empty($result_per_config)) {
      $form['validation_result_empty'] = ['#markup' => $this->t('No issues could be fixed.')];
    }
  }

  private function addResultRows(array &$table, array $items): void {
    foreach ($items as $key => $item) {
      assert($item instanceof ResultItemInterface);
      $table[$key]['type'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => ucfirst($item->getType()),
          ],
        ],
      ];
      $table[$key]['description'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => $item->getDescription(),
          ],
        ],
      ];
      $table[$key]['rule'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => $item->getRule()->getLabel(),
          ],
        ],
      ];
    }
  }

  private function getValidationDescription(): string {
    if ($this->configPolicy && $this->configRule) {
      return $this->t("You are about to validate the '%name' rule on the current configuration.", ['%name' => $this->configRule->getLabel()]);
    }
    elseif ($this->configPolicy) {
      $rule_count = $this->configPolicy->getRules()->count();
      return $this->formatPlural($rule_count, "You are about to validate one rule in the '%policy' policy.", "You are about to validate @count rules in the '%policy' policy.", ['%policy' => $this->configPolicy->getLabel()]);
    }
    else {
      return $this->t("You are about to validate all policy on the current configuration.");
    }
  }

  private function getRedirectUrl(): Url {
    if ($this->configPolicy && $this->configRule) {
      return Url::fromRoute('entity.config_policy.edit_form', ['config_policy' => $this->configPolicy->getId()]);
    }
    else {
      return Url::fromRoute('entity.config_policy.collection');
    }
  }

}
