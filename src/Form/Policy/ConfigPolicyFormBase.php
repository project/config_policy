<?php

namespace Drupal\config_policy\Form\Policy;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\config_policy\Entity\ConfigPolicyInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class ConfigPolicyFormBase extends EntityForm {
  protected $entityTypeManager;
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;
  protected PluginManagerInterface $configRulePluginManager;

  /**
   * Constructs a FieldPolicyForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  final public function __construct(EntityTypeManagerInterface $entityTypeManager, $configRulePluginManager, $entityTypeBundleInfo) {
    $this->entityTypeManager = $entityTypeManager;
    $this->configRulePluginManager = $configRulePluginManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.config_policy.rule'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $config_policy = $this->entity;

    assert($config_policy instanceof ConfigPolicyInterface);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $config_policy->label(),
      '#description' => $this->t("Provide a label so this policy can easily be identified."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $config_policy->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$config_policy->isNew(),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#maxlength' => 255,
      '#default_value' => $config_policy->getDescription(),
      '#description' => $this->t("Provide a description so other developers know what this policy does."),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Update config rule weights.
    if (!$form_state->isValueEmpty('rules')) {
      $this->updateRuleWeights($form_state->getValue('rules'));
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $config_policy = $this->entity;

    assert($config_policy instanceof ConfigPolicyInterface);

    $config_policy->setStatus(TRUE);
    $config_policy->setWeight(0);

    $status = parent::save($form, $form_state);

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label config policy was created.', [
        '%label' => $config_policy->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label config policy was updated.', [
        '%label' => $config_policy->label(),
      ]));
    }

    $form_state->setRedirect('entity.config_policy.collection');

    return $status;
  }

  /**
   * Updates rule weights.
   *
   * @param array $rules
   *   Associative array with effects having effect uuid as keys and array
   *   with effect data as values.
   */
  protected function updateRuleWeights(array $rules) {
    $config_policy = $this->entity;
    assert($config_policy instanceof ConfigPolicyInterface);

    foreach ($rules as $uuid => $rule_data) {
      if ($config_policy->getRules()->has($uuid)) {
        $config_policy->getRule($uuid)->setWeight($rule_data['weight']);
      }
    }
  }

  /**
   * Helper function to check whether a FieldPolicy configuration entity exists.
   */
  public function exist($id): bool {
    $entity = $this->entityTypeManager->getStorage('config_policy')->getQuery()
      ->condition('id', $id)
      ->accessCheck(FALSE)
      ->execute();
    return (bool) $entity;
  }

}
