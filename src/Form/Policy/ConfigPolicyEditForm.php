<?php

namespace Drupal\config_policy\Form\Policy;

use Drupal\Component\Utility\Unicode;
use Drupal\config_policy\Entity\ConfigPolicyInterface;
use Drupal\config_policy\Rule\ConfigRuleBase;
use Drupal\config_policy\Rule\ConfigRuleInterface;
use Drupal\config_policy\Rule\FixableRuleInterface;
use Drupal\config_policy\Rule\ValidatableRuleInterface;
use Drupal\config_policy\Rule\WeightedConfigRuleBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Form handler for the Example add and edit forms.
 */
final class ConfigPolicyEditForm extends ConfigPolicyFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $user_input = $form_state->getUserInput();

    $config_policy = $this->entity;
    assert($config_policy instanceof ConfigPolicyInterface);

    $form['rules'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Rule'),
        $this->t('Overridden'),
        $this->t('Description'),
        $this->t('Weight'),
        $this->t('Type'),
        $this->t('Operations'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'rule-order-weight',
        ],
      ],
      '#attributes' => [
        'id' => 'config_policy_rules',
      ],
      '#empty' => $this->t('There are currently no rules in this config policy. Add one by selecting an option below.'),
      '#weight' => 5,
    ];
    $this->addRuleRows($form, $config_policy, $user_input);
    $this->addNewRuleRow($form, $config_policy, $user_input);

    return $form;
  }

  /**
   * Add rule rows to the rules table.
   */
  private function addRuleRows(&$form, $config_policy, $user_input) {
    foreach ($config_policy->getRules() as $rule) {
      assert($rule instanceof ConfigRuleInterface);

      $key = $rule->getUuid();
      $form['rules'][$key]['#attributes']['class'][] = 'draggable';
      $form['rules'][$key]['#weight'] = isset($user_input['rules']) ? $user_input['rules'][$key]['weight'] : NULL;
      $form['rules'][$key]['rule'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => $rule->getConfiguration()['settings']['label'] ?? 'Undefined',
          ],
        ],
      ];

      $form['rules'][$key]['overriden'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => $rule instanceof WeightedConfigRuleBase && !$rule->isTopRule(get_class($rule)) ? $this->t('Yes') : $this->t('No'),
          ],
        ],
      ];

      $form['rules'][$key]['description'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => strlen($rule->getDescription()) > 50
              ? substr($rule->getDescription(), 0, 50) . "..."
              : $rule->getDescription(),
          ],
        ],
      ];

      $form['rules'][$key]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $rule->getLabel()]),
        '#title_display' => 'invisible',
        '#default_value' => $rule->getWeight(),
        '#attributes' => [
          'class' => ['rule-order-weight'],
        ],
      ];

      $form['rules'][$key]['type'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => $rule->getLabel(),
          ],
        ],
      ];

      $this->addRuleOperation($form, $config_policy, $rule, $key);
    }
  }

  public function addRuleOperation(&$form, $config_policy, $rule, $key) {
    $links = [];
    $is_configurable = $rule instanceof ConfigRuleBase;
    $is_validatable = $rule instanceof ValidatableRuleInterface;
    $is_fixable = $rule instanceof FixableRuleInterface;
    if ($is_configurable) {
      $links['edit'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('config_policy.rule_edit_form', [
          'config_policy' => $config_policy->id(),
          'config_rule' => $key,
        ]),
      ];
    }
    if ($is_validatable || $is_fixable) {
      $links['validate'] = [
        'title' => $this->t('Validate'),
        'url' => Url::fromRoute('config_policy.validate_form', [
          'config_policy' => $config_policy->id(),
          'config_rule' => $key,
        ]),
      ];
    }
    $links['delete'] = [
      'title' => $this->t('Delete'),
      'url' => Url::fromRoute('config_policy.rule_delete_form', [
        'config_policy' => $config_policy->id(),
        'config_rule' => $key,
      ]),
    ];
    $form['rules'][$key]['operations'] = [
      '#type' => 'operations',
      '#links' => $links,
    ];
  }

  public function addNewRuleRow(&$form, ConfigPolicyInterface $config_policy, $user_input) {
    $new_rules_options = [];
    $rules = $this->configRulePluginManager->getDefinitions();

    foreach ($rules as $rule => $definition) {
      $new_rules_options[$rule] = $definition['label'];
    }
    $form['rules']['new'] = [
      '#tree' => FALSE,
      '#weight' => $user_input['weight'] ?? NULL,
      '#attributes' => ['class' => ['draggable']],
    ];
    $form['rules']['new']['rule'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['container-inline'],
      ],
      'data' => [
        'new' => [
          '#type' => 'select',
          '#title' => $this->t('Rule'),
          '#title_display' => 'invisible',
          '#options' => $new_rules_options,
          '#empty_option' => $this->t('Select a new rule'),
        ],
        [
          'add' => [
            '#type' => 'submit',
            '#value' => $this->t('Add'),
            '#validate' => ['::ruleValidate'],
            '#submit' => ['::submitForm', '::ruleSave'],
          ],
        ],
      ],
    ];

    $form['rules']['new']['overridden'] = [
      '#tree' => FALSE,
      'data' => [
        'label' => [
          '#plain_text' => '',
        ],
      ],
    ];

    $form['rules']['new']['description'] = [
      '#tree' => FALSE,
      'data' => [
        'label' => [
          '#plain_text' => '',
        ],
      ],
    ];

    $form['rules']['new']['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight for new rule'),
      '#title_display' => 'invisible',
      '#default_value' => count($config_policy->getRules()) + 1,
      '#attributes' => ['class' => ['rule-order-weight']],
    ];
    $form['rules']['new']['operations'] = [
      'data' => [],
    ];

    uasort($rules, function ($a, $b) {
      return Unicode::strcasecmp($a['label'], $b['label']);
    });
  }

  /**
   * Validate handler for rules.
   */
  public function ruleValidate($form, FormStateInterface $form_state) {
    if (!$form_state->getValue('new')) {
      $form_state->setErrorByName('new', $this->t('Select a rule to add.'));
    }
  }

  /**
   * Submit handler for rules.
   */
  public function ruleSave($form, FormStateInterface $form_state) {
    $this->save($form, $form_state);
    $config_policy = $this->entity;

    assert($config_policy instanceof ConfigPolicyInterface);

    $this->getRequest()->query->set(
      'destination',
      Url::fromRoute(
        'config_policy.rule_add_form',
        [
          'config_policy' => $config_policy->id(),
          'config_rule' => $form_state->getValue('new'),
        ],
        ['query' => ['weight' => $form_state->getValue('weight')]]
      )->toString()
    );
  }

}
