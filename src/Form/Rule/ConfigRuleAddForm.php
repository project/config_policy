<?php

namespace Drupal\config_policy\Form\Rule;

use Drupal\config_policy\Entity\ConfigPolicyInterface;
use Drupal\config_policy\Rule\ConfigRulePluginManager;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an add form for config rules.
 *
 * @internal
 */
final class ConfigRuleAddForm extends ConfigRuleFormBase {

  /**
   * The config rule plugin manager.
   *
   * @var \Drupal\config_policy\Rule\ConfigRulePluginManager
   */
  protected ConfigRulePluginManager $configRulePluginManager;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ConfigPolicyInterface $config_policy = NULL, string $config_rule = NULL): array {
    $form = parent::buildForm($form, $form_state, $config_policy, $config_rule);

    $form['#title'] = $this->t('Add %label rule to policy %policy', [
      '%label' => $this->configRule->getLabel(),
      '%policy' => $config_policy->label(),
    ]);
    $form['actions']['submit']['#value'] = $this->t('Add rule');

    return $form;
  }

  protected function prepareConfigRule($config_rule): object {
    $config_rule = $this->configRulePluginManager->createInstance($config_rule);
    // Set the initial weight so this rule comes last.
    $config_rule->setWeight(count($this->configPolicy->getRules()));
    return $config_rule;
  }

}
