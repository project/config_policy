<?php

namespace Drupal\config_policy\Form\Rule;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Form for deleting a config rule.
 *
 * @internal
 */
class ConfigRuleDeleteForm extends ConfigRuleConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t(
      'Are you sure you want to delete the @rule rule from the %policy policy?',
      ['%policy' => $this->configPolicy->label(), '@rule' => $this->configRule->getLabel()]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'config_rule_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configPolicy->deleteRule($this->configRule);
    $this->messenger()->addStatus($this->t(
      'The rule %name has been deleted.',
      ['%name' => $this->configRule->getLabel()]
    ));
    $form_state->setRedirectUrl($this->configPolicy->toUrl('edit-form'));
  }

}
