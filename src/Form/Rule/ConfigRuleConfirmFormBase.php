<?php

namespace Drupal\config_policy\Form\Rule;

use Drupal\config_policy\Entity\ConfigPolicyInterface;
use Drupal\config_policy\Rule\ConfigRuleInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Form for confirming a config rule action.
 *
 * @internal
 */
abstract class ConfigRuleConfirmFormBase extends ConfirmFormBase {

  /**
   * The config policy containing the config rule to be deleted.
   *
   * @var \Drupal\config_policy\Entity\ConfigPolicyInterface
   */
  protected ConfigPolicyInterface $configPolicy;

  /**
   * The config rule to be deleted.
   *
   * @var \Drupal\config_policy\Rule\ConfigRuleInterface
   */
  protected ConfigRuleInterface $configRule;

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return $this->configPolicy->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    ConfigPolicyInterface $config_policy = NULL,
    $config_rule = NULL,
  ): array {
    $this->configPolicy = $config_policy;
    $this->configRule = $this->configPolicy->getRule($config_rule);

    return parent::buildForm($form, $form_state);
  }

}
