<?php

namespace Drupal\config_policy\Form\Rule;

use Drupal\config_policy\Entity\ConfigPolicyInterface;
use Drupal\config_policy\Rule\ConfigRuleInterface;
use Drupal\config_policy\Rule\ConfigRulePluginManager;
use Drupal\config_policy\Rule\FixableRuleInterface;
use Drupal\config_policy\Rule\PreventableRuleInterface;
use Drupal\config_policy\Rule\ValidatableRuleInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base form for config rules.
 */
abstract class ConfigRuleFormBase extends FormBase {

  protected EntityTypeManagerInterface $entityTypeManager;
  protected ConfigRulePluginManager $configRulePluginManager;


  /**
   * The config policy.
   *
   * @var \Drupal\config_policy\Entity\ConfigPolicyInterface
   */
  protected ConfigPolicyInterface $configPolicy;

  /**
   * The config rule.
   *
   * @var \Drupal\config_policy\Rule\ConfigRuleInterface
   */
  protected ConfigRuleInterface $configRule;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'config_rule_form';
  }

  /**
   * Constructs a config policy form object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\config_policy\Rule\ConfigRulePluginManager $usage_rule_plugin_manager
   */
  final public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigRulePluginManager $usage_rule_plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configRulePluginManager = $usage_rule_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.config_policy.rule')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\config_policy\Entity\ConfigPolicyInterface $config_policy
   *   The config policy.
   * @param string|null $config_rule
   *   The config rule ID.
   *
   * @return array
   *   The form structure.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function buildForm(array $form, FormStateInterface $form_state, ConfigPolicyInterface $config_policy = NULL, string $config_rule = NULL): array {
    $this->configPolicy = $config_policy;
    $this->configRule = $this->prepareConfigRule($config_rule);
    $request = $this->getRequest();

    $form['uuid'] = [
      '#type' => 'value',
      '#value' => $this->configRule->getUuid(),
    ];
    $form['id'] = [
      '#type' => 'value',
      '#value' => $this->configRule->getPluginId(),
    ];

    $form['settings'] = [];

    $form['settings']['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->configRule->getConfiguration()['settings']['label'] ?? '',
      '#description' => $this->t("Provide a label so this rule can easily be identified."),
      '#required' => TRUE,
    ];

    $pattern_options = [];
    foreach ($this->configRule->getConfigPatterns() as $pattern) {
      $pattern_options[] = [$pattern => $pattern];
    }
    if (!empty($pattern_options) > 0) {
      $form['settings']['config_pattern'] = [
        '#type' => 'select',
        '#multiple' => TRUE,
        '#title' => $this->t('Config patterns'),
        '#name' => 'config-patterns',
        '#disabled' => TRUE,
        '#input' => FALSE,
        '#options' => $pattern_options,
        '#value' => $this->configRule->getConfigPatterns(),
        '#description' => $this->t("This rule will only apply to config matching these patterns."),
        '#attributes' => [
          'disabled' => 'disabled',
          'readonly' => 'readonly',
        ],
      ];
    }

    $this->addRuntimeAllowedSettings($form, $form_state);

    $subform_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    $subform_state->set('config_policy', $config_policy);
    $form['settings'] = $this->configRule->buildConfigurationForm($form['settings'], $subform_state);
    $form['settings']['#tree'] = TRUE;

    $form['weight'] = [
      '#type' => 'hidden',
      '#value' => $request->query->has('weight') ? (int) $request->query->get('weight') : $this->configRule->getWeight(),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $this->configPolicy->toUrl('edit-form'),
      '#attributes' => ['class' => ['button']],
    ];

    return $form;
  }

  private function addRuntimeAllowedSettings(array &$form, FormStateInterface $form_state): void {
    if ($this->configRule instanceof ValidatableRuleInterface) {
      $form['settings']['validate_runtime'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Automatically validate rule on runtime'),
        '#default_value' => $this->configRule->getConfiguration()['settings']['validate_runtime'] ?? 1,
      ];
      $form['settings']['validate_import'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Validate config on import'),
        '#default_value' => $this->configRule->getConfiguration()['settings']['validate_import'] ?? 0,
      ];
    }
    if ($this->configRule instanceof FixableRuleInterface) {
      $form['settings']['fix_runtime'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Automatically fix rule on runtime'),
        '#default_value' => $this->configRule->getConfiguration()['settings']['fix_runtime'] ?? 0,
      ];
    }
    if ($this->configRule instanceof PreventableRuleInterface) {
      $form['settings']['prevent'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Alter forms to prevent this rule from failing.'),
        '#default_value' => $this->configRule->getConfiguration()['settings']['prevent'] ?? 0,
      ];
    }
  }

  public function rebuildForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->configRule->validateConfigurationForm($form['settings'], SubformState::createForSubform($form['settings'], $form, $form_state));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    $this->configRule->submitConfigurationForm($form['settings'], SubformState::createForSubform($form['settings'], $form, $form_state));

    $this->configRule->setWeight($form_state->getValue('weight'));
    if (!$this->configRule->getUuid()) {
      $this->configPolicy->addRule($this->configRule->getConfiguration());
    }
    else {
      $this->configPolicy->getRules()
        ->setInstanceConfiguration($this->configRule->getConfiguration()['uuid'], $this->configRule->getConfiguration());
    }
    $this->configPolicy->save();

    $this->messenger()->addStatus($this->t('The config rule was successfully saved.'));
    $form_state->setRedirectUrl($this->configPolicy->toUrl('edit-form'));
  }

  public function ajaxCallback(array &$form, FormStateInterface $form_state) {
    $this->configRule->ajaxCallback($form, $form_state);
    return $form['settings']['ajaxContainer'];
  }

  abstract protected function prepareConfigRule($config_rule);

}
