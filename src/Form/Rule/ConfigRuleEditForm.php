<?php

namespace Drupal\config_policy\Form\Rule;

use Drupal\config_policy\Entity\ConfigPolicyInterface;
use Drupal\config_policy\Rule\ConfigRuleInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an edit form for config rules.
 *
 * @internal
 */
class ConfigRuleEditForm extends ConfigRuleFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    ConfigPolicyInterface $config_policy = NULL,
    string $config_rule = NULL,
  ): array {
    $form = parent::buildForm($form, $form_state, $config_policy, $config_rule);

    $form['#title'] = $this->t('Edit %label rule on policy %style', [
      '%label' => $this->configRule->getLabel(),
      '%style' => $config_policy->label(),
    ]);
    $form['actions']['submit']['#value'] = $this->t('Update rule');

    return $form;
  }

  protected function prepareConfigRule($config_rule): ConfigRuleInterface {
    return $this->configPolicy->getRule($config_rule);
  }

}
