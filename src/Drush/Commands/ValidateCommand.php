<?php

namespace Drupal\config_policy\Drush\Commands;

use Drupal\config_policy\Policy\ConfigPolicyServiceInterface;
use Drupal\config_policy\Result\FixResultInterface;
use Drupal\config_policy\Result\ResultItem\ResultItemInterface;
use Drupal\config_policy\Result\ValidationResultInterface;
use Drupal\config_policy\Rule\FixableRuleInterface;
use Drush\Commands\DrushCommands;

class ValidateCommand extends DrushCommands {

  private ConfigPolicyServiceInterface $configPolicyService;

  public function __construct(ConfigPolicyServiceInterface $config_policy_service) {
    parent::__construct();
    $this->configPolicyService = $config_policy_service;
  }

  /**
   * Validate config using config policies.
   *
   * @param $prefix Config files matching this prefix will be validated.
   *
   * @command config-policy:validate
   * @aliases cpv
   * @option fix Attempt to fix invalid config.
   * @option sync Validate config from the sync folder.
   * @option y Do not ask for automatic fix confirmation.
   *
   * @usage drush cpv
   *   Validate all config currently loaded.
   * @usage drush cpv --sync
   *   Validate all config in the sync folder.
   * @usage drush cpv --fix
   *   Validate and fix invalid config, requires confirmation.
   * @usage drush cpv --fix --y
   *   Validate and fix invalid config, skip confirmation.
   * @usage drush cpv prefix
   *   Validate only config matching the prefix.
   *
   * @validate-module-enabled config_policy
   * @bootstrap configuration
   */
  public function validateConfig($prefix = '', $options = ['fix' => FALSE, 'y' => FALSE, 'sync' => FALSE]) {
    $validate_start = microtime(TRUE);
    $configs = $this->configPolicyService->getConfigFiles($options['sync'], $prefix);
    $validate_result = $this->configPolicyService->validate($configs);
    $validate_time_elapsed = round(microtime(TRUE) - $validate_start, 4);

    $this->printResultAsTable($validate_result);
    $this->printValidationSummary($validate_result, $validate_time_elapsed);

    if ($options['fix']) {
      $confirm_fix = $this->confirmAutomaticFix($validate_result, $options['y']);
      if ($confirm_fix) {
        $fix_start = microtime(TRUE);
        $fix_result = $this->configPolicyService->fix($configs, FALSE, $options['sync']);
        $fix_time_elapsed = round(microtime(TRUE) - $fix_start, 4);
        $this->printFixSummary($fix_result, $fix_time_elapsed);
        return $fix_result->isValid() ? 0 : 1;
      }
    }

    return $validate_result->isValid() ? 0 : 1;
  }

  public function printResultAsTable(ValidationResultInterface $result): void {
    $per_config_result = $result->groupResultsByConfig(TRUE);
    foreach ($per_config_result as $config_name => $items) {
      $this->writeln("<options=underscore>{$config_name}</>");
      $formatted_items = [];
      foreach ($items as $item) {
        assert($item instanceof ResultItemInterface);
        $type = $item->getType() === 'error' ? '<error>error</error>' : '<fg=white;bg=yellow>warning</>';
        $formatted_items[] = [
          $type,
          $item->getDescription(),
          $item->getRule()->getLabel(),
        ];
      }
      $this->io()->table(
        [],
        $formatted_items,
      );
    }
  }

  public function printValidationSummary(ValidationResultInterface $result, float $time_elapsed): void {
    $result_items = $result->getAll();
    $result_count = count($result_items);
    $error_count = count($result->get('error'));
    $warning_count = count($result->get('warning'));
    $ok_count = count($result->get('ok'));

    $this->io()->section('Validation summary');
    if ($error_count > 0 || $warning_count > 0) {
      $this->writeln("<error>{$error_count} errors</error>");
      $this->writeln("<fg=yellow>{$warning_count} warnings</>");
      $this->writeln("<fg=green>{$ok_count} valid</>");
    }
    else {
      $this->writeln('<fg=green;bg=green>All config is valid!</>');
    }
    $this->writeln('');
    $this->writeln("Validated {$result_count} config file(s) in {$time_elapsed}s");
  }

  public function printFixSummary(FixResultInterface $result, float $time_elapsed): void {
    $result_items = $result->getAll();
    $result_count = count($result_items);
    $error_count = count($result->get('error'));
    $warning_count = count($result->get('warning'));
    $fix_count = count($result->get('ok'));

    $this->io()->section('Automatic fix summary');
    if ($error_count > 0 || $warning_count > 0) {
      $this->writeln("<error>{$error_count} errors</error>");
      $this->writeln("<fg=yellow>{$warning_count} warnings</>");
      $this->writeln("<fg=green>{$fix_count} fixed</>");
    }
    else {
      $this->writeln("<fg=green;bg=green>All {$fix_count} issue(s) have been fixed.</>");
    }
    $this->writeln('');
    $this->writeln("Attempted to fix {$result_count} config file(s) in {$time_elapsed}s");
  }

  public function confirmAutomaticFix(ValidationResultInterface $validate_result, bool $skip_confirm): bool {
    $this->io()->writeln('');
    $fixable_items = array_filter($validate_result->getAll(), function ($item) {
      return $item->getRule() instanceof FixableRuleInterface
        && ($item->getType() === 'error' || $item->getType() === 'warning');
    });
    $fixable_items_count = count($fixable_items);
    if ($fixable_items_count === 0) {
      return FALSE;
    }
    $this->io()->section('Automatic fixing');
    $this->io()->writeln("<fg=white;bg=green>{$fixable_items_count} issue(s) can be automatically fixed.</>");
    $this->io()->block("Automatic fixing will change config, this is potentially destructive.", NULL, 'error', ' ', TRUE);
    $confirm = $skip_confirm || $this->io()
      ->confirm('Do you want to automatically fix any errors and warnings?');
    if (!$confirm) {
      $this->io()->block('Automatic fixing has been cancelled.', NULL, 'error');
    }
    return $confirm;
  }

}
