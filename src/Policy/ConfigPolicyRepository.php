<?php

namespace Drupal\config_policy\Policy;

use Drupal\config_policy\Rule\ConfigRulePluginManager;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

final class ConfigPolicyRepository implements ConfigPolicyRepositoryInterface {

  private EntityStorageInterface $storage;
  private ConfigRulePluginManager $rulePluginManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, ConfigRulePluginManager $rule_plugin_manager) {
    $this->storage = $entityTypeManager->getStorage('config_policy');
    $this->rulePluginManager = $rule_plugin_manager;
  }

  /**
   * Retrieves all enabled config policies.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function findAll(): array {
    return $this->storage->loadByProperties([
      'status' => TRUE,
    ]);
  }

  /**
   * Retrieves all config policies that have a specific rule.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function findByRule(string $class): array {
    return $this->storage->loadByProperties([
      'status' => TRUE,
      'rules.*.id' => $this->rulePluginManager->getIdByClass($class),
    ]);
  }

}
