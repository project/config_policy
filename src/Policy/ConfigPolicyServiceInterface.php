<?php

namespace Drupal\config_policy\Policy;

use Drupal\config_policy\Entity\ConfigPolicyInterface;
use Drupal\config_policy\Result\FixResultInterface;
use Drupal\config_policy\Result\ResultInterface;
use Drupal\config_policy\Result\ValidationResultInterface;
use Drupal\config_policy\Rule\ConfigRuleInterface;
use Drupal\Core\Config\Config;

interface ConfigPolicyServiceInterface {

  public function validate(array $configs, bool $on_runtime = FALSE, bool $on_import = FALSE, ConfigPolicyInterface $target_policy = NULL, ConfigRuleInterface $target_rule = NULL): ValidationResultInterface;

  public function fix(array $configs, bool $on_runtime = FALSE, bool $sync = FALSE, ConfigPolicyInterface $target_policy = NULL, ConfigRuleInterface $target_rule = NULL): FixResultInterface;

  public function isRuleApplicable(ConfigRuleInterface $rule, Config $config, ResultInterface $result): bool;

  public function getConfigFiles(bool $sync_folder, string $prefix = ''): array;

}
