<?php

namespace Drupal\config_policy\Policy;

use Drupal\config_policy\Entity\ConfigPolicyInterface;
use Drupal\config_policy\Result\FixResult;
use Drupal\config_policy\Result\FixResultInterface;
use Drupal\config_policy\Result\ResultInterface;
use Drupal\config_policy\Result\ResultItem\ErrorResultItem;
use Drupal\config_policy\Result\ValidationResult;
use Drupal\config_policy\Result\ValidationResultInterface;
use Drupal\config_policy\Rule\ConditionalRuleInterface;
use Drupal\config_policy\Rule\ConfigRuleInterface;
use Drupal\config_policy\Rule\FixableRuleInterface;
use Drupal\config_policy\Rule\PreventableRuleInterface;
use Drupal\config_policy\Rule\ValidatableRuleInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Exception;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class ConfigPolicyService implements ConfigPolicyServiceInterface {

  private ConfigPolicyRepositoryInterface $configPolicyRepository;
  private ConfigFactoryInterface $configFactory;
  private ConfigFactoryInterface $syncConfigFactory;

  public function __construct(
    ConfigPolicyRepositoryInterface $config_policy_repository,
    ConfigFactoryInterface $config_factory,
    StorageInterface $sync_storage,
    TypedConfigManagerInterface $typed_config_manager,
    EventDispatcherInterface $event_dispatcher,
  ) {
    $this->configPolicyRepository = $config_policy_repository;
    $this->configFactory = $config_factory;
    $this->syncConfigFactory = new ConfigFactory($sync_storage, $event_dispatcher, $typed_config_manager);
  }

  public function validateChangelist(array $changelist): ValidationResultInterface {
    $changed = array_merge($changelist['create'], $changelist['update'], $changelist['rename']);
    $configs = $this->syncConfigFactory->loadMultiple($changed);
    return $this->validate($configs, FALSE, TRUE);
  }

  /**
   * @param \Drupal\Core\Config\Config[] $configs
   * @param bool $on_runtime
   *
   * @return \Drupal\config_policy\Result\ValidationResultInterface
   */
  public function validate(array $configs, bool $on_runtime = FALSE, bool $on_import = FALSE, ConfigPolicyInterface $target_policy = NULL, ConfigRuleInterface $target_rule = NULL): ValidationResultInterface {
    $result = new ValidationResult();
    $validate_result = $this->iterateRules([$this, 'tryValidateRule'], $result, $configs, $on_runtime, FALSE, $target_policy, $target_rule);
    assert($validate_result instanceof ValidationResultInterface);
    return $validate_result;
  }

  public function fix(array $configs, bool $on_runtime = FALSE, bool $sync = FALSE, ConfigPolicyInterface $target_policy = NULL, ConfigRuleInterface $target_rule = NULL): FixResultInterface {
    $result = new FixResult();
    $fix_result = $this->iterateRules([$this, 'tryFixRule'], $result, $configs, $on_runtime, $sync, $target_policy, $target_rule);
    assert($fix_result instanceof FixResultInterface);
    return $fix_result;
  }

  public function prevent(array &$form, FormStateInterface $form_state, string $form_id): void {
    $policies = $this->configPolicyRepository->findAll();
    foreach ($policies as $policy) {
      $rules_collection = $policy->getRules();
      $rules = iterator_to_array($rules_collection);
      foreach ($rules as $rule) {
        assert($rule instanceof ConfigRuleInterface);
        if ($rule instanceof PreventableRuleInterface && $this->canPrevent($rule, $form_id)) {
          $rule->prevent($form, $form_state, $form_id);
        }
      }
    }
  }

  private function iterateRules(callable $function, ResultInterface $result, array $configs, bool $on_runtime = FALSE, bool $sync = FALSE, ConfigPolicyInterface $target_policy = NULL, ConfigRuleInterface $target_rule = NULL): ResultInterface {
    if ($target_rule) {
      $function($target_rule, $configs, $on_runtime, $sync, $result);
      return $result;
    }

    $policies = $this->configPolicyRepository->findAll();
    foreach ($policies as $policy) {
      if ($target_policy && $target_policy->getId() !== $policy->getId()) {
        continue;
      }
      $rules_collection = $policy->getRules();
      $rules = iterator_to_array($rules_collection);
      foreach ($rules as $rule) {
        $function($rule, $configs, $on_runtime, $sync, $result);
      }
    }
    return $result;
  }

  private function tryValidateRule(ConfigRuleInterface $rule, array $configs, bool $on_runtime, bool $on_import, ResultInterface $result): void {
    assert($result instanceof ValidationResultInterface);
    if ($on_runtime && !$this->canValidateOnRuntime($rule)) {
      return;
    }

    if ($on_import && !$this->canValidateOnImport($rule)) {
      return;
    }

    if (!$rule instanceof ValidatableRuleInterface) {
      return;
    }

    foreach ($configs as $config) {
      if (!$this->isRuleMatchingPattern($rule, $config)) {
        continue;
      }
      if (!$this->isRuleApplicable($rule, $config, $result)) {
        continue;
      }
      try {
        $rule->validate($config, $result);
      }
      catch (\Throwable $e) {
        $result->add(new ErrorResultItem($e->getMessage(), $config, $rule));
      }
    }
  }

  private function tryFixRule(ConfigRuleInterface $rule, array $configs, bool $on_runtime, bool $sync, ResultInterface $result): void {
    assert($result instanceof FixResultInterface);
    if ($on_runtime && !$this->canFixOnRuntime($rule)) {
      return;
    }

    if (!$rule instanceof FixableRuleInterface) {
      return;
    }

    foreach ($configs as $config) {
      if (!$this->isRuleMatchingPattern($rule, $config)) {
        continue;
      }
      if (!$this->isRuleApplicable($rule, $config, $result)) {
        continue;
      }

      if ($config instanceof ImmutableConfig) {
        $mutable_config = $sync
          ? $this->syncConfigFactory->getEditable($config->getName())
          : $this->configFactory->getEditable($config->getName());
        try {
          $rule->fix($mutable_config, $result);
        }
        catch (\Throwable $e) {
          $result->add(new ErrorResultItem($e->getMessage(), $config, $rule));
        }
      }
      else {
        try {
          $rule->fix($config, $result);
        }
        catch (\Throwable $e) {
          $result->add(new ErrorResultItem($e->getMessage(), $config, $rule));
        }
      }
    }
  }

  private function canValidateOnRuntime(ConfigRuleInterface $rule): bool {
    return $rule->getSettings()['validate_runtime'];
  }

  private function canValidateOnImport(ConfigRuleInterface $rule): bool {
    return $rule->getSettings()['validate_import'];
  }

  private function canFixOnRuntime(ConfigRuleInterface $rule): bool {
    return $rule->getSettings()['fix_runtime'];
  }

  private function canPrevent(ConfigRuleInterface $rule, string $form_id): bool {
    return in_array($form_id, $rule->getPreventableForms())
      && $rule->getSettings()['prevent'];
  }

  private function isRuleMatchingPattern(ConfigRuleInterface $rule, Config $config): bool {
    $config_patterns = $rule->getConfigPatterns();
    $config_name = $config->getName();
    foreach ($config_patterns as $pattern) {
      if (self::wildcardMatch($pattern, $config_name)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Checks if a string matches a given wildcard pattern.
   *
   * @param string $pattern
   *   The wildcard pattern to me matched.
   * @param string $string
   *   The string to be checked.
   *
   * @return bool
   *   TRUE if $string string matches the $pattern pattern.
   */
  protected static function wildcardMatch($pattern, $string) {
    $pattern = '/^' . preg_quote($pattern, '/') . '$/';
    $pattern = str_replace('\*', '.*', $pattern);
    return (bool) preg_match($pattern, $string);
  }

  public function isRuleApplicable(ConfigRuleInterface $rule, Config $config, ResultInterface $result): bool {
    try {
      return !$rule instanceof ConditionalRuleInterface
        || $rule->applies($config);
    }
    catch (\Throwable $e) {
      $result->add(new ErrorResultItem($e->getMessage(), $config, $rule));
      return FALSE;
    }
  }

  /**
   * @throws \Exception
   */
  public function getConfigFiles(bool $sync_folder, string $prefix = ''): array {
    $config_names = $sync_folder ? $this->syncConfigFactory->listAll($prefix) : $this->configFactory->listAll($prefix);
    if (count($config_names) === 0) {
      throw new Exception('No config found matching the prefix.');
    }
    return $sync_folder ? $this->syncConfigFactory->loadMultiple($config_names) : $this->configFactory->loadMultiple($config_names);
  }

}
