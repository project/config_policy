<?php

namespace Drupal\config_policy\Policy;

interface ConfigPolicyRepositoryInterface {

  /**
   * Retrieves all enabled config policies.
   *
   * @return \Drupal\config_policy\Entity\ConfigPolicyInterface[]
   */
  public function findAll(): array;

  /**
   * Retrieves all config policies that have a specific rule.
   *
   * @return \Drupal\config_policy\Entity\ConfigPolicyInterface[]
   */
  public function findByRule(string $class): array;

}
