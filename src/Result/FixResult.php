<?php

namespace Drupal\config_policy\Result;

class FixResult extends ResultBase implements FixResultInterface {

  public function isValid(): bool {
    return $this->count('error') === 0;
  }

}
