<?php

namespace Drupal\config_policy\Result;

class ValidationResult extends ResultBase implements ValidationResultInterface {

  public function isValid(): bool {
    return $this->count('error') === 0;
  }

}
