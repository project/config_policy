<?php

namespace Drupal\config_policy\Result;

use Drupal\config_policy\Result\ResultItem\ResultItemInterface;

interface ResultInterface {

  public function add(ResultItemInterface $result_item): void;

  /**
   * @return \Drupal\config_policy\Result\ResultItem\ResultItemInterface[]
   */
  public function getAll(): array;

  /**
   * @return \Drupal\config_policy\Result\ResultItem\ResultItemInterface[]
   */
  public function get(string $type): array;

  public function countAll(): int;

  public function count(string $type): int;

  public function groupResultsByConfig(bool $skip_ok): array;

}
