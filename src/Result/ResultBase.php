<?php

namespace Drupal\config_policy\Result;

use Drupal\config_policy\Result\ResultItem\ResultItemInterface;

abstract class ResultBase implements ResultInterface {

  /**
   * @var \Drupal\config_policy\Result\ResultItem\ResultItemInterface[]
   */
  private array $items = [];

  public function add(ResultItemInterface $result_item): void {
    $this->items[] = $result_item;
  }

  /**
   * @return \Drupal\config_policy\Result\ResultItem\ResultItemInterface[]
   */
  public function getAll(): array {
    return $this->items;
  }

  /**
   * @return \Drupal\config_policy\Result\ResultItem\ResultItemInterface[]
   */
  public function get(string $type): array {
    return array_filter($this->items, function ($item) use ($type) {
      return $item->getType() === $type;
    });
  }

  public function countAll(): int {
    return count($this->items);
  }

  public function count(string $type): int {
    return count($this->get($type));
  }

  public function groupResultsByConfig(bool $skip_ok): array {
    $grouped_by_config = [];
    foreach ($this->getAll() as $item) {
      if ($item->getType() === 'ok' && $skip_ok) {
        continue;
      }
      $config_name = $item->getConfig()->getName();
      $existing = $grouped_by_config[$config_name] ?? [];
      array_push($existing, $item);
      $grouped_by_config[$config_name] = $existing;
    }
    return $grouped_by_config;
  }

}
