<?php

namespace Drupal\config_policy\Result;

interface ValidationResultInterface extends ResultInterface {

  public function isValid(): bool;

}
