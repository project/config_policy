<?php

namespace Drupal\config_policy\Result;

interface FixResultInterface extends ResultInterface {

  public function isValid(): bool;

}
