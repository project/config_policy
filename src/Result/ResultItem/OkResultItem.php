<?php

namespace Drupal\config_policy\Result\ResultItem;

class OkResultItem extends ResultItemBase {

  private string $type = 'ok';
  private bool $isFatal = FALSE;

  public function getType(): string {
    return $this->type;
  }

  public function isFatal(): bool {
    return $this->isFatal;
  }

}
