<?php

namespace Drupal\config_policy\Result\ResultItem;

use Drupal\config_policy\Rule\ConfigRuleInterface;
use Drupal\Core\Config\Config;

interface ResultItemInterface {

  public function __construct(string $description, Config $config, ConfigRuleInterface $rule);

  public function getDescription(): string;

  public function getConfig(): Config;

  public function getRule(): ConfigRuleInterface;

  public function getType(): string;

  public function isFatal(): bool;

}
