<?php

namespace Drupal\config_policy\Result\ResultItem;

use Drupal\config_policy\Rule\ConfigRuleInterface;
use Drupal\Core\Config\Config;

abstract class ResultItemBase implements ResultItemInterface {

  private string $description;
  private Config $config;
  private ConfigRuleInterface $rule;

  public function __construct(string $description, Config $config, ConfigRuleInterface $rule) {
    $this->description = $description;
    $this->config = $config;
    $this->rule = $rule;
  }

  public function getDescription(): string {
    return $this->description;
  }

  public function getConfig(): Config {
    return $this->config;
  }

  public function getRule(): ConfigRuleInterface {
    return $this->rule;
  }

}
