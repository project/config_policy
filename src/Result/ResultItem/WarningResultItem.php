<?php

namespace Drupal\config_policy\Result\ResultItem;

class WarningResultItem extends ResultItemBase {

  private string $type = 'warning';
  private bool $isFatal = FALSE;

  public function getType(): string {
    return $this->type;
  }

  public function isFatal(): bool {
    return $this->isFatal;
  }

}
