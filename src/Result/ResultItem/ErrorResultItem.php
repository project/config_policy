<?php

namespace Drupal\config_policy\Result\ResultItem;

class ErrorResultItem extends ResultItemBase {

  private string $type = 'error';
  private bool $isFatal = TRUE;

  public function getType(): string {
    return $this->type;
  }

  public function isFatal(): bool {
    return $this->isFatal;
  }

}
