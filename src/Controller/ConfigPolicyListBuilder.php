<?php

namespace Drupal\config_policy\Controller;

use Drupal\config_policy\Entity\ConfigPolicyInterface;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a listing of config policies on the field level.
 */
class ConfigPolicyListBuilder extends DraggableListBuilder {

  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage) {
    parent::__construct($entity_type, $storage);
    $this->weightKey = 'weight';
    $this->entitiesKey = 'config_policies';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Config Policy');
    $header['description'] = $this->t('Description');
    $header['rules'] = $this->t('Rules');
    $header['status']['data'] = $this->t('Status');
    $header['weight'] = $this->t('Weight');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    assert($entity instanceof ConfigPolicyInterface);

    $row['label'] = $entity->label();
    $row['description'] = [
      '#plain_text' => strlen($entity->getDescription()) > 50
        ? substr($entity->getDescription(), 0, 50) . "..."
        : $entity->getDescription(),
    ];
    $row['rules'] = ['#plain_text' => count($entity->getRules())];
    $row['status'] = [
      '#plain_text' => $entity->status() == 1 ? $this->t('Enabled') : $this->t('Disabled'),
    ];

    return $row + parent::buildRow($entity);
  }

  public function getOperations(EntityInterface $entity) {
    assert($entity instanceof ConfigPolicyInterface);

    $operations = parent::getOperations($entity);

    if (count($entity->getRules()->getValidatableRules()) > 0 || count($entity->getRules()->getFixableRules()) > 0) {
      $operations['validate'] = [
        'title' => $this->t('Validate'),
        'weight' => -20,
        'url' => $this->ensureDestination(Url::fromRoute('config_policy.policy_validate_form', [
          'config_policy' => $entity->id(),
        ])),
      ];
    }

    if (!$entity->status()) {
      $operations['enable'] = [
        'title' => $this->t('Enable'),
        'weight' => 40,
        'url' => Url::fromRoute(
          'entity.config_policy.enable',
          ['config_policy' => $entity->id(), 'type' => $entity->getEntityTypeId()]
        ),
        'attributes' => [
          'class' => 'use-ajax',
        ],
      ];
    }
    else {
      $operations['disable'] = [
        'title' => $this->t('Disable'),
        'weight' => 40,
        'url' => Url::fromRoute(
          'entity.config_policy.disable',
          ['config_policy' => $entity->id(), 'type' => $entity->getEntityTypeId()]
        ),
        'attributes' => [
          'class' => 'use-ajax',
        ],
      ];
    }

    return $operations;
  }

  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    if (count($this->getEntityIds()) > 0) {
      $form['actions']['validate'] = [
        '#type' => 'link',
        '#title' => $this->t('Validate all'),
        '#url' => Url::fromRoute('config_policy.global_validate_form'),
        '#attributes' => ['class' => ['button']],
      ];
    }
    return $form;
  }

  public static function redirect(&$form, &$form_state) {
    $form_state['redirect'] = Url::fromRoute('entity.config_policy.add_form');
  }

  public function getFormId(): string {
    return 'config_policy_list';
  }

}
