<?php

namespace Drupal\config_policy\Controller;

use Drupal\config_policy\Entity\ConfigPolicyInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

class ConfigPolicyController extends ControllerBase {

  public function disable(Request $request, $config_policy, $type): AjaxResponse {
    $response = new AjaxResponse();
    $found_policy = $this->entityTypeManager()->getStorage($type)->load($config_policy);

    assert($found_policy instanceof ConfigPolicyInterface);

    $found_policy->setStatus(FALSE);
    $found_policy->save();
    return $response->addCommand(new RedirectCommand($request->headers->get('referer')));
  }

  public function enable(Request $request, $config_policy, $type): AjaxResponse {
    $response = new AjaxResponse();
    $found_policy = $this->entityTypeManager()->getStorage($type)->load($config_policy);

    assert($found_policy instanceof ConfigPolicyInterface);

    $found_policy->setStatus(TRUE);
    $found_policy->save();
    return $response->addCommand(new RedirectCommand($request->headers->get('referer')));
  }

}
