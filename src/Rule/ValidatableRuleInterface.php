<?php

namespace Drupal\config_policy\Rule;

use Drupal\config_policy\Result\ValidationResultInterface;
use Drupal\Core\Config\Config;

interface ValidatableRuleInterface {

  public function validate(Config $config, ValidationResultInterface $result): ValidationResultInterface;

}
