<?php

namespace Drupal\config_policy\Rule;

class WeightedConfigRuleBase extends ConfigRuleBase implements WeightedRuleInterface {

  public function isTopRule(string $class): bool {
    $policies_with_rule = $this->configPolicyRepository->findByRule($class);
    if (empty($policies_with_rule)) {
      return TRUE;
    }
    usort($policies_with_rule, fn ($a, $b) => $a->getWeight() <=> $b->getWeight());
    $top_policy = reset($policies_with_rule);
    $rules = $top_policy->getRules()->findAllByClass($class);
    $rules = array_filter($rules, fn ($rule) => !$this->differs($rule));
    if (empty($rules)) {
      return TRUE;
    }
    usort($rules, fn ($a, $b) => $a->getWeight() <=> $b->getWeight());
    $top_rule = reset($rules);
    return $top_rule->getUuid() === $this->getUuid();
  }

  public function differs(ConfigRuleInterface $rule): bool {
    return FALSE;
  }

}
