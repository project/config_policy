<?php

namespace Drupal\config_policy\Rule;

use Drupal\config_policy\Result\FixResultInterface;
use Drupal\Core\Config\Config;

interface FixableRuleInterface {

  public function fix(Config $config, FixResultInterface $result): FixResultInterface;

}
