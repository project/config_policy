<?php

namespace Drupal\config_policy\Rule;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

interface ConfigRuleInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface, PluginFormInterface {

  public function getSummary(): array;

  public function getUuid(): string;

  public function getWeight(): string;

  public function setWeight($weight): void;

  public function getLabel(): string;

  public function getDescription(): string;

  public function getConfigPatterns(): array;

  public function getPreventableForms(): array;

  public function getSettings(): array;

  public function ajaxCallback(array &$form, FormStateInterface $form_state): void;

}
