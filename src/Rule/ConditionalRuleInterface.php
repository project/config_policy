<?php

namespace Drupal\config_policy\Rule;

use Drupal\Core\Config\Config;

interface ConditionalRuleInterface {

  public function applies(Config $config): bool;

}
