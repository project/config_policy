<?php

namespace Drupal\config_policy\Rule;

use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * A collection of rules.
 */
class ConfigRulePluginCollection extends DefaultLazyPluginCollection {

  /**
   * {@inheritdoc}
   */
  public function sortHelper($aID, $bID): int {
    return $this->get($aID)->getWeight() <=> $this->get($bID)->getWeight();
  }

  public function getFixableRules(): array {
    return array_filter(iterator_to_array(
      $this->getIterator()),
      fn ($rule) => $rule instanceof FixableRuleInterface
    );
  }

  public function getValidatableRules(): array {
    return array_filter(iterator_to_array(
      $this->getIterator()),
      fn ($rule) => $rule instanceof ValidatableRuleInterface
    );
  }

  public function findAllByClass(string $class): array {
    return array_filter(iterator_to_array($this->getIterator()), fn ($rule) => $class === get_class($rule));
  }

}
