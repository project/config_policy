<?php

namespace Drupal\config_policy\Rule;

use DomainException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Traversable;

class ConfigRulePluginManager extends DefaultPluginManager {
  protected FieldTypePluginManagerInterface $fieldTypeManager;

  public function __construct(Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ConfigPolicyRule',
      $namespaces,
      $module_handler,
      'Drupal\config_policy\Rule\ConfigRuleInterface',
      'Drupal\config_policy\Annotation\ConfigPolicyRule',
    );

    $this->alterInfo('config_rule_info');
    $this->setCacheBackend($cache_backend, 'config_rule_plugins');
  }

  public function getIdByClass(string $class): string {
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if ($definition['class'] === $class) {
        return $plugin_id;
      }
    }

    throw new DomainException(sprintf('No rule found for %s', $class));
  }

}
