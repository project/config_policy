<?php

namespace Drupal\config_policy\Rule;

use Drupal\Core\Form\FormStateInterface;

interface PreventableRuleInterface {

  public function prevent(array &$form, FormStateInterface $form_state, string $form_id): void;

}
