<?php

namespace Drupal\config_policy\Rule;

interface WeightedRuleInterface {

  public function isTopRule(string $class): bool;

  public function differs(ConfigRuleInterface $rule): bool;

}
