<?php

namespace Drupal\config_policy\Rule;

use Drupal\config_policy\Policy\ConfigPolicyRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConfigRuleBase extends PluginBase implements ConfigRuleInterface, ContainerFactoryPluginInterface {

  protected string $uuid;

  protected string $weight = '';

  protected LoggerInterface $logger;

  protected EntityTypeManagerInterface $entityTypeManager;

  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  protected ConfigPolicyRepositoryInterface $configPolicyRepository;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, ConfigPolicyRepositoryInterface $config_policy_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->configPolicyRepository = $config_policy_repository;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ConfigRuleBase {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('config_policy'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('config_policy.repository'),
    );
  }

  public function getSummary(): array {
    return [
      '#markup' => '',
      '#rule' => [
        'id' => $this->pluginDefinition['id'],
        'label' => $this->getLabel(),
        'description' => $this->pluginDefinition['description'],
      ],
    ];
  }

  public function getLabel(): string {
    return $this->pluginDefinition['label'];
  }

  public function getDescription(): string {
    return $this->pluginDefinition['description'];
  }

  public function getUuid(): string {
    return $this->uuid;
  }

  public function getWeight(): string {
    return $this->weight;
  }

  public function setWeight($weight): void {
    $this->weight = $weight;
  }

  public function getConfigPatterns(): array {
    return $this->pluginDefinition['configPatterns'];
  }

  public function getPreventableForms(): array {
    return $this->pluginDefinition['preventableForms'];
  }

  public function getSettings(): array {
    return $this->configuration;
  }

  public function getConfiguration(): array {
    return [
      'uuid' => $this->getUuid(),
      'id' => $this->getPluginId(),
      'weight' => $this->getWeight(),
      'settings' => $this->configuration,
    ];
  }

  public function setConfiguration(array $configuration): ConfigRuleBase {
    $configuration += [
      'settings' => [],
      'uuid' => '',
      'weight' => '',
    ];
    $this->configuration = $configuration['settings'] + $this->defaultConfiguration();
    $this->uuid = $configuration['uuid'];
    $this->weight = $configuration['weight'];
    return $this;
  }

  public function defaultConfiguration(): array {
    return [];
  }

  public function calculateDependencies(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  public function ajaxCallback(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['label'] = $form_state->getValue('label');
    $this->configuration['validate_runtime'] = $form_state->getValue(['validate_runtime']) ?? TRUE;
    $this->configuration['fix_runtime'] = $form_state->getValue(['fix_runtime']) ?? FALSE;
    $this->configuration['prevent'] = $form_state->getValue(['prevent']) ?? FALSE;
    $this->configuration['validate_import'] = $form_state->getValue(['validate_import']) ?? FALSE;
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    return $form;
  }

}
