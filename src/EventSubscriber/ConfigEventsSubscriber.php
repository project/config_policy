<?php

namespace Drupal\config_policy\EventSubscriber;

use Drupal\config_policy\Policy\ConfigPolicyService;
use Drupal\config_policy\Result\ResultItem\ResultItemInterface;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigImporterEvent;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ConfigEventsSubscriber implements EventSubscriberInterface {

  private ConfigPolicyService $configPolicyService;
  private MessengerInterface $messenger;
  private array $importingConfig = [];

  public function __construct(ConfigPolicyService $config_policy_service, MessengerInterface $messenger) {
    $this->configPolicyService = $config_policy_service;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      ConfigEvents::IMPORT_VALIDATE => 'configImportValidate',
      ConfigEvents::SAVE => 'configSave',
    ];
  }

  public function configImportValidate(ConfigImporterEvent $event) {
    $changed = [];
    foreach ($event->getChangelist() as $action) {
      $changed = array_merge($changed, $action);
    }
    $this->importingConfig = $changed;

    $validation_result = $this->configPolicyService->validateChangelist($event->getChangelist());
    foreach ($validation_result->get('error') as $result_item) {
      assert($result_item instanceof ResultItemInterface);
      $event->getConfigImporter()->logError('Invalid config: ' . $result_item->getConfig()->getName() . ' -> ' . $result_item->getDescription());
    }
  }

  public function configSave(ConfigCrudEvent $event) {
    if (($key = array_search($event->getConfig()->getName(), $this->importingConfig)) !== FALSE) {
      unset($this->importingConfig[$key]);
      return;
    }
    $validation_result = $this->configPolicyService->validate([$event->getConfig()], TRUE);
    $this->configPolicyService->fix([$event->getConfig()], TRUE);
    if ($validation_result->count('error') > 0) {
      $result_items = $validation_result->get('error');
      $result_item = reset($result_items);
      $this->messenger->addError($result_item->getDescription());
    }
    if ($validation_result->count('warning') > 0) {
      $result_items = $validation_result->get('warning');
      $result_item = reset($result_items);
      $this->messenger->addWarning($result_item->getDescription());
    }
  }

}
