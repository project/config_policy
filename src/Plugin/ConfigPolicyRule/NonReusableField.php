<?php

namespace Drupal\config_policy\Plugin\ConfigPolicyRule;

use Drupal\config_policy\Rule\ConfigRuleBase;
use Drupal\config_policy\Rule\PreventableRuleInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Makes fields with names starting with a content type non-reusable.
 *
 * @ConfigPolicyRule(
 *   id = "non_reusable_field",
 *   label = @Translation("Non-reusable fields"),
 *   configPatterns = {},
 *   preventableForms = {
 *     "field_ui_field_storage_add_form",
 *   },
 *   description = @Translation("Makes fields with names starting with a content type non-reusable.")
 * )
 */
class NonReusableField extends ConfigRuleBase implements PreventableRuleInterface {

  public function prevent(array &$form, FormStateInterface $form_state, string $form_id): void {
    if (!isset($form['add']['existing_storage_name']['#options'])) {
      return;
    }
    $form['add']['existing_storage_name']['#options'] = array_filter(
        $form['add']['existing_storage_name']['#options'],
        fn ($key) => !preg_match('/[a-z0-9_]+__[a-z0-9_]+/', $key),
        ARRAY_FILTER_USE_KEY
      );
  }

}
