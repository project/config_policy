<?php

namespace Drupal\config_policy\Plugin\ConfigPolicyRule;

use Drupal\config_policy\Policy\ConfigPolicyRepositoryInterface;
use Drupal\config_policy\Result\FixResultInterface;
use Drupal\config_policy\Result\ResultItem\ErrorResultItem;
use Drupal\config_policy\Result\ResultItem\OkResultItem;
use Drupal\config_policy\Result\ValidationResultInterface;
use Drupal\config_policy\Rule\ConfigRuleBase;
use Drupal\config_policy\Rule\FixableRuleInterface;
use Drupal\config_policy\Rule\PreventableRuleInterface;
use Drupal\config_policy\Rule\ValidatableRuleInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Prevents installing modules that are disallowed.
 *
 * @ConfigPolicyRule(
 *   id = "modules_disallowed",
 *   label = @Translation("Disallowed modules"),
 *   description = @Translation("Prevents installing modules that are disallowed."),
 *   configPatterns = {
 *     "core.extension"
 *   },
 *   preventableForms = {
 *     "system_modules"
 *   }
 * )
 */
class ModulesDisallowedRule extends ConfigRuleBase implements ValidatableRuleInterface, FixableRuleInterface, PreventableRuleInterface {

  protected ModuleInstallerInterface $moduleInstaller;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, ConfigPolicyRepositoryInterface $config_policy_repository, ModuleInstallerInterface $module_installer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger, $entity_type_manager, $entity_type_bundle_info, $config_policy_repository);
    $this->moduleInstaller = $module_installer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ConfigRuleBase {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('config_policy'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('config_policy.repository'),
      $container->get('module_installer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate(Config $config, ValidationResultInterface $result): ValidationResultInterface {
    $invalid = $this->getInvalidModules($config);
    if (empty($invalid)) {
      $result->add(new OkResultItem("No disallowed modules installed", $config, $this));

      return $result;
    }

    foreach ($invalid as $module) {
      $result->add(new ErrorResultItem("Disallowed module: $module.", $config, $this));
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function fix(Config $config, FixResultInterface $result): FixResultInterface {
    $invalid = $this->getInvalidModules($config);
    foreach ($invalid as $module) {
      $this->moduleInstaller->uninstall([$module]);
      $result->add(new OkResultItem("The module $module has been uninstalled.", $config, $this));
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function prevent(array &$form, FormStateInterface $form_state, string $form_id): void {
    $disallowed = $this->getDisallowedModules();

    foreach (Element::children($form['modules']) as $category) {
      foreach (Element::children($form['modules'][$category]) as $module) {
        if (in_array($module, $disallowed)) {
          $form['modules'][$category][$module]['enable']['#disabled'] = TRUE;
        }
      }
    }
  }

  protected function getDisallowedModules(): array {
    return $this->configuration['disallowed'] ?? [];
  }

  protected function getInvalidModules(Config $config): array {
    $installed = $config->get('module') ?? [];

    return array_intersect(
      array_keys($installed),
      $this->getDisallowedModules()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['disallowed'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Disallowed modules'),
      '#description' => $this->t('Enter the names of the modules to disallow, separated by commas.'),
      '#default_value' => implode(', ', $this->configuration['disallowed'] ?? []),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state):  void {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['disallowed'] = array_map('trim', explode(',', $form_state->getValue('disallowed')));
  }

}
