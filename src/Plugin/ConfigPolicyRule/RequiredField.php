<?php

namespace Drupal\config_policy\Plugin\ConfigPolicyRule;

use Drupal\config_policy\Result\FixResultInterface;
use Drupal\config_policy\Result\ResultItem\ErrorResultItem;
use Drupal\config_policy\Result\ResultItem\OkResultItem;
use Drupal\config_policy\Result\ValidationResultInterface;
use Drupal\config_policy\Rule\ConditionalRuleInterface;
use Drupal\config_policy\Rule\ConfigRuleBase;
use Drupal\config_policy\Rule\FixableRuleInterface;
use Drupal\config_policy\Rule\ValidatableRuleInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Requires a field to be present on an entity.
 *
 * @ConfigPolicyRule(
 *   id = "required_field",
 *   label = @Translation("Required field"),
 *   configPatterns = {
 *     "field.storage.*.*"
 *   },
 *   preventableForms = { },
 *   description = @Translation("Requires a field to be present on an entity.")
 * )
 */
class RequiredField extends ConfigRuleBase implements ValidatableRuleInterface, FixableRuleInterface, ConditionalRuleInterface {

  public function applies(Config $config): bool {
    $field_storage = $this->loadFieldStorage();

    $config_id = $field_storage->id();
    $field_id = $config->get('id');

    return $config_id === $field_id;
  }

  public function validate(Config $config, ValidationResultInterface $result): ValidationResultInterface {
    $field_storage = $this->loadFieldStorage();
    assert($field_storage instanceof FieldStorageConfigInterface);
    $field_name = $field_storage->getName();

    $expected_entity_type = $this->configuration['entity_type'];
    $expected_bundles = empty($this->configuration['bundles']) ? array_keys($this->loadBundleInfo($expected_entity_type)) : $this->configuration['bundles'];

    $actual_bundles = array_keys($field_storage->getBundles());
    $diff = array_diff($expected_bundles, $actual_bundles);

    if (!empty($diff)) {
      $bundle_list = implode(', ', $diff);
      $result->add(new ErrorResultItem("The '$field_name' field is not present at the '$expected_entity_type' entity type and the following bundles: $bundle_list.", $config, $this));
    }
    else {
      $result->add(new OkResultItem('The field was added to all required entities.', $config, $this));
    }

    return $result;
  }

  public function fix(Config $config, FixResultInterface $result): FixResultInterface {
    $field_storage = $this->loadFieldStorage();
    assert($field_storage instanceof FieldStorageConfigInterface);
    $field_name = $field_storage->getName();

    $expected_entity_type = $this->configuration['entity_type'];
    $expected_bundles = empty($this->configuration['bundles']) ? array_keys($this->loadBundleInfo($expected_entity_type)) : $this->configuration['bundles'];

    $actual_bundles = array_keys($field_storage->getBundles());
    $diff = array_diff($expected_bundles, $actual_bundles);

    if (!empty($diff)) {
      foreach ($diff as $bundle) {
        $label = $this->configuration['fieldLabel'] ?? ucfirst($field_name);
        $field = FieldConfig::create([
          'field_storage' => $field_storage,
          'bundle' => $bundle,
          'label' => $label,
        ]);
        $field->save();
        $result->add(new OkResultItem("Added the field '$field_name' to bundle '$bundle' with label '$label' and on entity type '$expected_entity_type'.", $config, $this));
      }
    }

    return $result;
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#description' => $this->t("Select entity type to further scope this rule."),
      '#options' => $this->loadEntityTypes(),
      '#default_value' => $this->configuration['entity_type'] ?? '',
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'disable-refocus' => FALSE,
        'prevent' => 'click',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => '',
        ],
        'wrapper' => 'ajax_wrapper',
      ],
    ];
    $form['entity_types_update'] = [
      '#type' => 'submit',
      '#value' => $this->t('Set entity type'),
      '#submit' => ['::rebuildForm'],
      '#limit_validation_errors' => [
        ['settings', 'entity_type'],
      ],
      '#attributes' => ['class' => ['js-hide']],
    ];
    $form['ajaxContainer'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'ajax_wrapper',
      ],
    ];
    $form['ajaxContainer']['bundles'] = [
      '#type' => 'select',
      '#title' => $this->t('Bundles'),
      '#description' => $this->t("Select bundles to apply this rule to specific bundles only."),
      '#options' => $this->loadBundleInfo($this->configuration['entity_type'] ?? ''),
      '#multiple' => TRUE,
      '#validated' => TRUE,
      '#default_value' => $this->configuration['bundles'] ?? [],
      '#access' => ($this->configuration['entity_type'] ?? NULL) !== NULL,
    ];

    $form['ajaxContainer']['field'] = [
      '#title' => $this->t('Field to require'),
      '#description' => $this->t('Select the field to be required.'),
      '#required' => TRUE,
      '#validated' => TRUE,
      '#type' => 'select',
      '#options' => $this->loadFieldStorageConfigs($this->configuration['entity_type'] ?? ''),
      '#default_value' => $this->configuration['field'] ?? '',
    ];

    $form['fieldLabel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field label'),
      '#description' => $this->t('The label a new field should be given when auto fixed.'),
      '#maxlength' => 128,
      '#default_value' => $this->configuration['fieldLabel'] ?? '',
    ];

    return $form;
  }

  public function ajaxCallback(array &$form, FormStateInterface $form_state): void {
    $selected_entity_type = $form_state->getValue('settings')['entity_type'];
    $form['settings']['ajaxContainer']['bundles']['#options'] = $this->loadBundleInfo($selected_entity_type);
    $form['settings']['ajaxContainer']['bundles']['#access'] = $selected_entity_type !== '';
    $form['settings']['ajaxContainer']['field']['#options'] = $this->loadFieldStorageConfigs($selected_entity_type);
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['entity_type'] = $form_state->getValue(['entity_type']);
    $this->configuration['bundles'] = array_keys($form_state->getValue(['ajaxContainer', 'bundles']) ?? []);
    $this->configuration['field'] = $form_state->getValue(['ajaxContainer', 'field']);
    $this->configuration['fieldLabel'] = $form_state->getValue(['fieldLabel']);
  }

  protected function loadEntityTypes(): array {
    $type_definitions = [];
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if (!$definition->getBundleOf()) {
        continue;
      }
      $type_definitions[$definition->getBundleOf()] = $definition->getLabel();
    }
    return ['' => $this->t('- Select -')] + $type_definitions;
  }

  protected function loadBundleInfo($entity_type_id): array {
    $bundle_info = [];
    foreach ($this->entityTypeBundleInfo->getBundleInfo($entity_type_id) as $bundle_id => $bundle) {
      $bundle_info[$bundle_id] = $bundle['label'];
    }
    return $bundle_info;
  }

  protected function loadFieldStorageConfigs($entity_type_id): array {
    $options = [];
    $field_storage_storage = $this->entityTypeManager->getStorage('field_storage_config');
    $field_storage_config_ids = $field_storage_storage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('entity_type', $entity_type_id, '=')
      ->execute();

    $field_storage_configs = $field_storage_storage->loadMultiple($field_storage_config_ids);
    foreach ($field_storage_configs as $id => $field_storage_config) {
      assert($field_storage_config instanceof FieldStorageConfigInterface);
      $options[$id] = $field_storage_config->id();
    }
    return ['' => $this->t('- Select -')] + $options;
  }

  protected function loadFieldStorage(): EntityInterface {
    $field = $this->configuration['field'];
    return $this->entityTypeManager->getStorage('field_storage_config')->load($field);
  }

}
