<?php

namespace Drupal\config_policy\Plugin\ConfigPolicyRule;

use Drupal\config_policy\Result\ResultItem\ErrorResultItem;
use Drupal\config_policy\Result\ValidationResultInterface;
use Drupal\config_policy\Rule\ConditionalRuleInterface;
use Drupal\config_policy\Rule\ConfigRuleInterface;
use Drupal\config_policy\Rule\ValidatableRuleInterface;
use Drupal\config_policy\Rule\WeightedConfigRuleBase;
use Drupal\Core\Config\Config;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Keeps a field at the top or bottom of a display mode.
 *
 * @ConfigPolicyRule(
 *   id = "entity_view_display_order",
 *   label = @Translation("Display mode field order"),
 *   configPatterns = {
 *     "core.entity_view_display.*.*.*"
 *   },
 *   preventableForms = {
 *     "entity_view_display_edit_form"
 *   },
 *   description = @Translation("Keeps a field at the top or bottom of a display mode.")
 * )
 */
class EntityViewDisplayWeight extends WeightedConfigRuleBase implements ValidatableRuleInterface, ConditionalRuleInterface {

  public function applies(Config $config): bool {
    $is_top_rule = parent::isTopRule(self::class);
    if (!$is_top_rule) {
      return FALSE;
    }

    $fields = $this->configuration['fields'];
    $included_count = count(array_filter($fields, fn ($field) => $field['validate']));
    if ($included_count === 0) {
      return FALSE;
    }

    $bundle = $this->configuration['bundle'];
    $entity_type = $this->configuration['entity_type'];

    $view_mode_id = $config->get('id');
    $view_modes = $this->configuration['view_modes'];
    if (!empty($view_modes) && !in_array($view_mode_id, $view_modes)) {
      return FALSE;
    }

    $config_bundle = $config->get('bundle');
    $config_entity_type = $config->get('targetEntityType');
    return $config_entity_type === $entity_type && $config_bundle === $bundle;
  }

  public function differs(ConfigRuleInterface $rule): bool {
    return $this->configuration['bundle'] !== $rule->getConfiguration()['settings']['bundle']
      || $this->configuration['entity_type'] !== $rule->getConfiguration()['settings']['entity_type'];
  }

  public function validate(Config $config, ValidationResultInterface $result): ValidationResultInterface {
    $fields = $this->configuration['fields'];
    uasort($fields, fn ($a, $b) => $a['weight'] <=> $b['weight']);
    $included_fields = array_filter($fields, fn ($field) => $field['validate']);
    $included_enabled_fields = array_filter($included_fields, fn ($field) => !$field['disabled']);

    $current_fields = $config->get('content');
    uasort($current_fields, fn ($a, $b) => $a['weight'] <=> $b['weight']);
    $included_current_fields = array_filter($current_fields, fn ($field) => in_array($field, array_keys($included_fields)), ARRAY_FILTER_USE_KEY);

    foreach ($included_fields as $name => $included_field) {
      $current_included_field = $included_current_fields[$name] ?? NULL;
      if ($included_field['disabled'] && $current_included_field) {
        $result->add(new ErrorResultItem("The field '$name' needs to be disabled.", $config, $this));
        continue;
      }
      $expected_index = array_search($name, array_keys($included_enabled_fields));
      $index = array_search($name, array_keys($current_fields));
      if ($expected_index !== FALSE && $index !== FALSE && $index !== $expected_index) {
        $result->add(new ErrorResultItem("The field '$name' needs to be at position '$expected_index'.", $config, $this));
      }
    }

    return $result;
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#description' => $this->t("Select an entity type to apply this rule on."),
      '#options' => $this->loadEntityTypes(),
      '#default_value' => $this->configuration['entity_type'] ?? '',
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'disable-refocus' => FALSE,
        'prevent' => 'click',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => '',
        ],
        'wrapper' => 'ajax_wrapper',
      ],
    ];
    $form['entity_type_update'] = [
      '#type' => 'submit',
      '#value' => $this->t('Set entity type'),
      '#submit' => ['::rebuildForm'],
      '#limit_validation_errors' => [
        ['settings', 'entity_type'],
      ],
      '#attributes' => ['class' => ['js-hide']],
    ];
    $form['ajaxContainer'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'ajax_wrapper',
      ],
    ];
    $form['ajaxContainer']['bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Bundle'),
      '#description' => $this->t("Select a bundle to apply this rule on."),
      '#options' => $this->loadBundleInfo($this->configuration['entity_type'] ?? ''),
      '#validated' => TRUE,
      '#required' => TRUE,
      '#default_value' => $this->configuration['bundle'] ?? '',
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'disable-refocus' => FALSE,
        'prevent' => 'click',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => '',
        ],
        'wrapper' => 'ajax_wrapper',
      ],
    ];

    $form['ajaxContainer']['fields'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('ID'),
        $this->t('Disabled'),
        $this->t('Validate'),
        $this->t('Weight'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'field-order-weight',
        ],
      ],
      '#attributes' => [
        'id' => 'config_policy_rule_field_order',
      ],
      '#empty' => $this->t('There are no fields found for this entity type & bundle.'),
    ];
    $user_input = $form_state->getUserInput();
    $this->loadFieldStorageConfigs($form['ajaxContainer']['fields'], $user_input);

    $form['ajaxContainer']['view_modes'] = [
      '#type' => 'select',
      '#title' => $this->t('View modes'),
      '#description' => $this->t("Select view modes to limit this rule to only those."),
      '#default_value' => $this->configuration['view_modes'] ?? [],
      '#options' => $this->loadEntityViewModes($this->configuration['entity_type'] ?? NULL, $this->configuration['bundle'] ?? NULL),
      '#validated' => TRUE,
      '#multiple' => TRUE,
    ];

    return $form;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['fields'] = $form_state->getValue(['ajaxContainer', 'fields']);
    $this->configuration['entity_type'] = $form_state->getValue(['entity_type']);
    $this->configuration['bundle'] = $form_state->getValue(['ajaxContainer', 'bundle']);
    $this->configuration['view_modes'] = $form_state->getValue(['ajaxContainer', 'view_modes']);
  }

  private function loadFieldStorageConfigs(array &$table, $user_input, string $entity_type = NULL, string $bundle = NULL): void {
    $entity_type ??= $this->configuration['entity_type'] ?? '';
    $bundle ??= $this->configuration['bundle'] ?? '';
    if (empty($entity_type) || empty($bundle)) {
      return;
    }
    $field_storage_config_ids = $this->entityTypeManager
      ->getStorage('field_config')->getQuery()
      ->condition('status', TRUE, '=')
      ->condition('entity_type', $entity_type, '=')
      ->condition('bundle', $bundle, '=')
      ->execute();
    $field_storage_configs = $this->entityTypeManager->getStorage('field_config')->loadMultiple($field_storage_config_ids);
    $default_view_modes = $this->entityTypeManager->getStorage('entity_view_display')
      ->loadByProperties([
        'targetEntityType' => $entity_type,
        'bundle' => $bundle,
        'mode' => 'default',
      ]);
    $default_view_mode = reset($default_view_modes);
    assert($default_view_mode instanceof EntityViewDisplay);

    if (!empty($this->configuration['fields'])) {
      // @phpstan-ignore-next-line
      usort($field_storage_configs, fn($a, $b) => $this->configuration['fields'][$a->get('field_name')]['weight'] <=> $this->configuration['fields'][$b->get('field_name')]['weight']);
    }

    foreach ($field_storage_configs as $field_storage_config) {
      assert($field_storage_config instanceof FieldStorageConfigInterface);
      $key = $field_storage_config->get('field_name');
      $table[$key]['#attributes']['class'][] = 'draggable';
      $table[$key]['#weight'] = isset($user_input['fields']) ? $user_input['fields'][$key]['weight'] : NULL;

      $table[$key]['Name'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => $field_storage_config->get('field_name'),
          ],
        ],
      ];
      $table[$key]['ID'] = [
        '#tree' => FALSE,
        'data' => [
          'label' => [
            '#plain_text' => $field_storage_config->get('id'),
          ],
        ],
      ];
      $table[$key]['disabled'] = [
        '#type' => 'checkbox',
        '#default_value' => $this->configuration['fields'][$key]['disabled'] ?? 0,
      ];
      $table[$key]['validate'] = [
        '#type' => 'checkbox',
        '#default_value' => $this->configuration['fields'][$key]['validate'] ?? 0,
      ];
      $table[$key]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $field_storage_config->getLabel()]),
        '#title_display' => 'invisible',
        '#default_value' => $default_view_mode->get('content')[$field_storage_config->get('field_name')]['weight'] ?? 0,
        '#attributes' => [
          'class' => ['field-order-weight'],
        ],
      ];
    }
  }

  public function ajaxCallback(array &$form, FormStateInterface $form_state): void {
    $selected_entity_type = $form_state->getValue('settings')['entity_type'];
    $selected_bundle = $form_state->getValue('settings')['ajaxContainer']['bundle'];
    $user_input = $form_state->getUserInput();
    $form['settings']['ajaxContainer']['bundle']['#options'] = $this->loadBundleInfo($selected_entity_type);
    $form['settings']['ajaxContainer']['view_modes']['#options'] = $this->loadEntityViewModes($selected_entity_type, $selected_bundle);
    $this->loadFieldStorageConfigs($form['settings']['ajaxContainer']['fields'], $user_input, $selected_entity_type, $selected_bundle);
  }

  protected function loadEntityTypes(): array {
    $type_definitions = [];
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if (!$definition->getBundleOf()) {
        continue;
      }
      $type_definitions[$definition->getBundleOf()] = $definition->getLabel();
    }
    return ['' => $this->t('- Select -')] + $type_definitions;
  }

  protected function loadBundleInfo(string $entity_type_id): array {
    $bundle_info = [];
    foreach ($this->entityTypeBundleInfo->getBundleInfo($entity_type_id) as $bundle_id => $bundle) {
      $bundle_info[$bundle_id] = $bundle['label'];
    }
    return ['' => $this->t('- Select -')] + $bundle_info;
  }

  private function loadEntityViewModes(string $entity_type = NULL, string $bundle = NULL): array {
    $view_mode_options = [];
    if ($entity_type === '') {
      $entity_type = $this->configuration['entity_type'] ?? '';
    }
    if ($bundle === '') {
      $bundle = $this->configuration['bundle'] ?? '';
    }
    if (empty($entity_type) || empty($bundle)) {
      return [];
    }
    $view_modes = $this->entityTypeManager->getStorage('entity_view_mode')->getQuery()
      ->condition('targetEntityType', $entity_type, '=')
      ->accessCheck(FALSE)
      ->execute();
    $view_modes[] = $entity_type . '.default';
    sort($view_modes);
    foreach ($view_modes as $view_mode) {
      $view_mode_options += [$view_mode => $view_mode];
    }
    return $view_mode_options;
  }

}
