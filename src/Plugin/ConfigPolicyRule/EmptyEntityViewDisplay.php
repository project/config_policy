<?php

namespace Drupal\config_policy\Plugin\ConfigPolicyRule;

use Drupal\config_policy\Result\FixResultInterface;
use Drupal\config_policy\Result\ResultItem\ErrorResultItem;
use Drupal\config_policy\Result\ResultItem\OkResultItem;
use Drupal\config_policy\Result\ValidationResultInterface;
use Drupal\config_policy\Rule\ConditionalRuleInterface;
use Drupal\config_policy\Rule\ConfigRuleBase;
use Drupal\config_policy\Rule\FixableRuleInterface;
use Drupal\config_policy\Rule\PreventableRuleInterface;
use Drupal\config_policy\Rule\ValidatableRuleInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

/**
 * Keeps a certain display mode always empty.
 *
 * @ConfigPolicyRule(
 *   id = "empty_entity_view_display",
 *   label = @Translation("Empty display mode"),
 *   configPatterns = {
 *     "core.entity_view_display.*.*.*"
 *   },
 *   preventableForms = {
 *     "entity_view_display_edit_form"
 *   },
 *   description = @Translation("Keeps a certain display mode empty.")
 * )
 */
class EmptyEntityViewDisplay extends ConfigRuleBase implements ValidatableRuleInterface, ConditionalRuleInterface, FixableRuleInterface, PreventableRuleInterface {

  public function applies(Config $config): bool {
    $entity_types = $this->configuration['entity_types'];
    $bundles = $this->configuration['bundles'];
    $bundle = $config->get('bundle');
    $bundle_entity_type = $config->get('targetEntityType');

    if (empty($entity_types) && empty($bundles)) {
      return TRUE;
    }
    elseif (in_array($bundle_entity_type, $entity_types) && in_array($bundle, $bundles)) {
      return TRUE;
    }
    elseif (empty($bundles) && in_array($bundle_entity_type, $entity_types)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public function validate(Config $config, ValidationResultInterface $result): ValidationResultInterface {
    $view_modes = $this->configuration['view_modes'];
    $target_view_mode = $config->get('targetEntityType') . '.' . $config->get('mode');

    if (!in_array($target_view_mode, $view_modes)) {
      return $result;
    }

    if ($config->get('content') !== []) {
      $result->add(new ErrorResultItem('Content inside this display mode is not allowed.', $config, $this));
    }
    else {
      $result->add(new OkResultItem('Display mode was empty.', $config, $this));
    }
    return $result;
  }

  public function fix(Config $config, FixResultInterface $result): FixResultInterface {
    $view_modes = $this->configuration['view_modes'];
    $target_view_mode = $config->get('targetEntityType') . '.' . $config->get('mode');

    if (!in_array($target_view_mode, $view_modes)) {
      return $result;
    }

    if ($config->get('content') !== []) {
      $config->set('content', []);
      $config->save();
      $result->add(new OkResultItem('Display mode has been emptied.', $config, $this));
    }
    return $result;
  }

  public function prevent(array &$form, FormStateInterface $form_state, string $form_id): void {
    $form['#validate'][] = [self::class, 'validateEmptyDisplayMode'];
  }

  public static function validateEmptyDisplayMode(array $form, FormStateInterface $form_state) {
    $fields = $form_state->getValue('fields');
    foreach ($fields as $field) {
      if ($field['region'] === 'content') {
        $form_state->setError($form['fields'], t("This display mode can't have any content."));
      }
    }
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['entity_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity types'),
      '#description' => $this->t("Select entity types to further scope this rule."),
      '#options' => $this->loadEntityTypes(),
      '#default_value' => $this->configuration['entity_types'] ?? [],
      '#multiple' => TRUE,
      '#ajax' => [
        'callback' => '::ajaxCallback',
        'disable-refocus' => FALSE,
        'prevent' => 'click',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => '',
        ],
        'wrapper' => 'ajax_wrapper',
      ],
    ];
    $form['entity_types_update'] = [
      '#type' => 'submit',
      '#value' => $this->t('Set entity type'),
      '#submit' => ['::rebuildForm'],
      '#limit_validation_errors' => [
        ['settings', 'entity_types'],
      ],
      '#attributes' => ['class' => ['js-hide']],
    ];
    $form['ajaxContainer'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'ajax_wrapper',
      ],
    ];
    $form['ajaxContainer']['bundles'] = [
      '#type' => 'select',
      '#title' => $this->t('Bundles'),
      '#description' => $this->t("Select bundles to apply this rule to specific bundles only."),
      '#options' => $this->loadBundleInfo($this->configuration['entity_types'] ?? []),
      '#multiple' => TRUE,
      '#validated' => TRUE,
      '#default_value' => $this->configuration['bundles'] ?? [],
    ];

    $form['ajaxContainer']['view_modes'] = [
      '#type' => 'select',
      '#title' => $this->t('View modes'),
      '#description' => $this->t("The view modes that will be kept empty."),
      '#default_value' => $this->configuration['view_modes'] ?? [],
      '#options' => $this->loadEntityViewModes($this->configuration['entity_types'] ?? []),
      '#validated' => TRUE,
      '#multiple' => TRUE,
    ];

    return $form;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['entity_types'] = array_keys($form_state->getValue(['entity_types']) ?? []);
    $this->configuration['bundles'] = array_keys($form_state->getValue(['ajaxContainer', 'bundles']) ?? []);
    $this->configuration['view_modes'] = array_keys($form_state->getValue(['ajaxContainer', 'view_modes']));
  }

  public function ajaxCallback(array &$form, FormStateInterface $form_state): void {
    $selected_entity_types = $form_state->getValue('settings')['entity_types'];
    $form['settings']['ajaxContainer']['bundles']['#options'] = $this->loadBundleInfo($selected_entity_types);
    $form['settings']['ajaxContainer']['bundles']['#access'] = count($selected_entity_types) > 0;
    $form['settings']['ajaxContainer']['view_modes']['#options'] = $this->loadEntityViewModes($selected_entity_types);
    $form['settings']['ajaxContainer']['view_modes']['#access'] = count($selected_entity_types) > 0;
  }

  protected function loadEntityTypes(): array {
    $type_definitions = [];
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if (!$definition->getBundleOf()) {
        continue;
      }
      $type_definitions[$definition->getBundleOf()] = $definition->getLabel();
    }
    return $type_definitions;
  }

  protected function loadBundleInfo($entity_type_ids): array {
    $bundle_info = [];
    foreach ($entity_type_ids as $entity_type_id) {
      foreach ($this->entityTypeBundleInfo->getBundleInfo($entity_type_id) as $bundle_id => $bundle) {
        $bundle_info[$bundle_id] = $bundle['label'];
      }
    }
    return $bundle_info;
  }

  private function loadEntityViewModes(array $entity_types = NULL): array {
    $view_mode_options = [];
    if ($entity_types === NULL) {
      $entity_types = $this->configuration['entity_types'] ?? [];
    }
    if (empty($entity_types)) {
      return [];
    }
    $view_modes = $this->entityTypeManager->getStorage('entity_view_mode')->getQuery()
      ->condition('targetEntityType', $entity_types, 'IN')
      ->accessCheck(FALSE)
      ->execute();
    foreach ($entity_types as $entity_type) {
      $view_modes[] = $entity_type . '.default';
    }
    sort($view_modes);
    foreach ($view_modes as $view_mode) {
      $view_mode_options += [$view_mode => $view_mode];
    }
    return $view_mode_options;
  }

}
