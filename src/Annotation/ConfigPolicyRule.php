<?php

namespace Drupal\config_policy\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a ConfigPolicyRule annotation object.
 *
 * @Annotation
 */
class ConfigPolicyRule extends Plugin {

  public string $id;
  public string $label;
  public string $description;
  public array $configPatterns;
  public ?array $preventableForms = [];
  public ?int $weight = NULL;

}
