<?php

namespace Drupal\config_policy\Entity;

use Drupal\config_policy\Rule\ConfigRuleInterface;
use Drupal\config_policy\Rule\ConfigRulePluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Config Policy entity.
 */
interface ConfigPolicyInterface extends ConfigEntityInterface {

  public function getId(): string;

  public function getLabel(): string;

  public function getWeight(): int;

  public function setWeight(int $weight): void;

  public function setRules(array $rules);

  public function getRules(): ConfigRulePluginCollection;

  public function getRule(string $rule): ConfigRuleInterface;

  public function addRule(array $configuration): string;

  public function deleteRule(ConfigRuleInterface $config_rule): ConfigPolicyInterface;

  public function setDescription(string $description);

  public function getDescription(): string;

}
