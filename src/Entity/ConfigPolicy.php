<?php

namespace Drupal\config_policy\Entity;

use Drupal;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\config_policy\Rule\ConfigRuleInterface;
use Drupal\config_policy\Rule\ConfigRulePluginCollection;
use Drupal\config_policy\Rule\ConfigRulePluginManager;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Defines the FieldPolicy entity for fields.
 *
 * @ConfigEntityType(
 *   id = "config_policy",
 *   label = @Translation("Config Policy"),
 *   handlers = {
 *     "list_builder" = "Drupal\config_policy\Controller\ConfigPolicyListBuilder",
 *     "form" = {
 *       "add" = "Drupal\config_policy\Form\Policy\ConfigPolicyAddForm",
 *       "edit" = "Drupal\config_policy\Form\Policy\ConfigPolicyEditForm",
 *       "delete" = "Drupal\config_policy\Form\Policy\ConfigPolicyDeleteForm",
 *       "validate" = "Drupal\config_policy\Form\Policy\ConfigPolicyValidateForm",
 *     }
 *   },
 *   config_prefix = "field",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "status",
 *     "rules",
 *     "weight",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/add",
 *     "edit-form" = "/admin/structure/{config_policy}",
 *     "delete-form" = "/admin/structure/{config_policy}/delete",
 *     "validate-form" = "/admin/structure/{config_policy}/validate",
 *     "disable" = "/admin/structure/{config_policy}/disable",
 *     "enable" = "/admin/structure/{config_policy}/enable",
 *   }
 * )
 */
class ConfigPolicy extends ConfigEntityBase implements ConfigPolicyInterface, EntityWithPluginCollectionInterface {

  protected string $id;
  protected string $label;
  protected string $description;
  protected int $weight;
  protected array $rules = [];
  protected ?ConfigRulePluginManager $usageRulePluginManager;
  protected ?ConfigRulePluginCollection $rulesCollection;

  public function setId(string $id) {
    $this->id = $id;
  }

  public function getId(): string {
    return $this->id;
  }

  public function getLabel(): string {
    return $this->label;
  }

  public function getWeight(): int {
    return $this->weight;
  }

  public function setWeight(int $weight): void {
    $this->weight = $weight;
  }

  public function setDescription(string $description) {
    $this->description = $description;
  }

  public function getDescription(): string {
    return $this->description ?? '';
  }

  public function setRules(array $rules) {
    $this->rules = $rules;
  }

  public function getRule($rule): ConfigRuleInterface {
    return $this->getRules()->get($rule);
  }

  public function getRules(): ConfigRulePluginCollection {
    if (!isset($this->rulesCollection)) {
      $this->rulesCollection = new ConfigRulePluginCollection($this->getUsageRulePluginManager(), $this->rules);
      $this->rulesCollection->sort();
    }
    return $this->rulesCollection;
  }

  public function addRule(array $configuration): string {
    $configuration['uuid'] = $this->uuidGenerator()->generate();
    $this->getRules()->addInstanceId($configuration['uuid'], $configuration);
    return $configuration['uuid'];
  }

  public function deleteRule(ConfigRuleInterface $config_rule): ConfigPolicyInterface {
    $this->getRules()->removeInstanceId($config_rule->getUuid());
    $this->save();
    return $this;
  }

  protected function getUsageRulePluginManager(): PluginManagerInterface {
    if (!isset($this->usageRulePluginManager)) {
      $this->usageRulePluginManager = Drupal::service('plugin.manager.config_policy.rule');
    }
    return $this->usageRulePluginManager;
  }

  public function getPluginCollections(): array {
    return ['rules' => $this->getRules()];
  }

}
