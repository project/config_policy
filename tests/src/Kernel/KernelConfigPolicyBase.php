<?php

namespace Drupal\Tests\config_policy\Kernel;

use Drupal\config_policy\Policy\ConfigPolicyRepositoryInterface;
use Drupal\config_policy\Policy\ConfigPolicyServiceInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;

abstract class KernelConfigPolicyBase extends KernelTestBase {
  use NodeCreationTrait, ContentTypeCreationTrait;

  protected ConfigPolicyRepositoryInterface $configPolicyRepository;

  protected ConfigPolicyServiceInterface $configPolicyService;
  protected ConfigFactoryInterface $configFactory;

  protected string $policyDescription = 'My description';

  protected function setUp(): void {
    parent::setUp();
    $this->enableModules(['config_policy', 'field', 'node', 'user', 'system', 'text', 'filter']);
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installEntitySchema('field_config');
    $this->installSchema('node', 'node_access');
    $this->installConfig(['config_policy', 'field', 'system', 'node', 'filter']);
    $this->configPolicyRepository = $this->container->get('config_policy.repository');
    $this->configPolicyService = $this->container->get('config_policy.service');
    $this->configFactory = $this->container->get('config.factory');
  }

  protected function addPolicy(string $id, string $label, string $rule_id, string $rule_label, int $status = 1): void {
    $config_policy = $this->container->get('entity_type.manager')
      ->getStorage('config_policy')
      ->create([
        'id' => $id,
        'label' => $label,
        'description' => $this->policyDescription,
        'status' => $status,
        'weight' => 0,
        'rules' => [
          $rule_id => [
            'uuid' => $rule_id,
            'id' => 'empty_entity_view_display',
            'weight' => 1,
            'settings' => [
              'label' => $rule_label,
              'entity_types' => ['node'],
              'bundles' => ['test_bundle'],
              'view_modes' => ['node.default'],
              'validate_runtime' => 1,
              'fix_runtime' => 0,
              'prevent' => 0,
              'validate_import' => 0,
            ],
          ],
        ],
      ]);
    $config_policy->save();
  }

  protected function createIssue() {
    $this->createContentType([
      'type' => 'test_bundle',
      'name' => 'Test node type',
    ]);
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_test_content',
      'entity_type' => 'node',
      'type' => 'text_long',
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'test_bundle',
    ]);
    $field->save();
    $this->createNode([
      'type' => 'test_bundle',
      'title' => 'Test title',
      'field_test_content' => 'long text',
    ]);
    $display_modes = $this->container->get('entity_type.manager')
      ->getStorage('entity_view_display')
      ->loadByProperties([
        'id' => 'node.test_bundle.default',
      ]);
    $display_mode = reset($display_modes);
    assert($display_mode instanceof EntityViewDisplayInterface);
    $display_mode->set('content', [
      'test_content' => [
        'settings' => [],
        'third_party_settings' => [],
        'weight' => 100,
        'region' => 'content',
      ],
    ]);
    $display_mode->save();
  }

}
