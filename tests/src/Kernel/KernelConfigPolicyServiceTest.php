<?php

namespace Drupal\Tests\config_policy\Kernel;

/**
 * @group Kernel
 */
class KernelConfigPolicyServiceTest extends KernelConfigPolicyBase {

  private function getConfig(): array {
    $config_names = $this->configFactory->listAll();
    return $this->configFactory->loadMultiple($config_names);
  }

  public function testValidate() {
    $this->createIssue();
    $this->addPolicy('test_policy', 'Test policy', 'empty_entity_view_display', 'Empty display mode');

    $result = $this->configPolicyService->validate($this->getConfig());

    $error_count = count($result->get('error'));
    $this->assertEquals(1, $error_count);
  }

  public function testFix() {
    $this->createIssue();
    $this->addPolicy('test_policy', 'Test policy', 'empty_entity_view_display', 'Empty display mode');

    $result = $this->configPolicyService->fix($this->getConfig());

    $fixed_count = count($result->get('ok'));
    $this->assertEquals(1, $fixed_count);
  }

  public function testGetConfigFiles() {
    $actual_config_files = $this->getConfig();
    $actual_config_count = count($actual_config_files);

    $config_files = $this->configPolicyService->getConfigFiles(FALSE);
    $config_count = count($config_files);

    $this->assertEquals($actual_config_count, $config_count);
  }

  public function testGetConfigFilesEmpty() {
    $this->expectExceptionMessage('No config found matching the prefix.');
    $this->configPolicyService->getConfigFiles(FALSE, 'non-existing-prefix');
  }

}
