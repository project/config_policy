<?php

namespace Drupal\Tests\config_policy\Kernel;

use Drupal\config_policy\Entity\ConfigPolicyInterface;
use Drupal\config_policy\Plugin\ConfigPolicyRule\EmptyEntityViewDisplay;

/**
 * @group Kernel
 */
class KernelConfigPolicyRepositoryTest extends KernelConfigPolicyBase {

  public function testFindAll(): void {
    $all_policy = $this->configPolicyRepository->findAll();
    $policy_count = count($all_policy);
    $this->assertEquals(0, $policy_count);

    $policy_id = 'test_policy';
    $this->addPolicy($policy_id, 'Test policy', 'test_rule', 'Test rule');

    $all_policy = $this->configPolicyRepository->findAll();
    $policy_count = count($all_policy);
    $policy = reset($all_policy);
    assert($policy instanceof ConfigPolicyInterface);

    $this->assertEquals(1, $policy_count);
    $this->assertEquals($policy->getId(), $policy_id);
  }

  public function testFindAllStatus(): void {
    $this->addPolicy('disabled_policy', 'Disabled policy', 'test_rule', 'Test rule', 0);
    $all_policy = $this->configPolicyRepository->findAll();
    $policy_count = count($all_policy);
    $this->assertEquals(0, $policy_count);

    $enabled_policy_id = 'enabled_policy';
    $this->addPolicy($enabled_policy_id, 'Enabled policy', 'test_rule', 'Test rule', 1);
    $all_policy = $this->configPolicyRepository->findAll();
    $policy_count = count($all_policy);
    $policy = reset($all_policy);
    assert($policy instanceof ConfigPolicyInterface);

    $this->assertEquals(1, $policy_count);
    $this->assertEquals($policy->getId(), $enabled_policy_id);
  }

  public function testFindByRule(): void {
    $matching_id = 'empty_entity_view_display';
    $this->addPolicy('not_matching_policy', 'Not matching', 'some_rule', 'Some rule', 0);
    $matching_policy = $this->configPolicyRepository->findByRule(EmptyEntityViewDisplay::class);
    $policy_count = count($matching_policy);
    $this->assertEquals(0, $policy_count);

    $matching_policy_id = 'matching_policy';
    $this->addPolicy($matching_policy_id, 'Matching policy', $matching_id, 'Test rule');
    $all_policy = $this->configPolicyRepository->findByRule(EmptyEntityViewDisplay::class);
    $policy_count = count($all_policy);
    $policy = reset($all_policy);
    assert($policy instanceof ConfigPolicyInterface);

    $this->assertEquals(1, $policy_count);
    $this->assertEquals($policy->getId(), $matching_policy_id);
  }

}
