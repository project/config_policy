<?php

use Drupal\Tests\config_policy\Functional\UiConfigPolicyBase;

/**
 * @group Browser
 */
class ConfigPolicyUiConfigRuleTest extends UiConfigPolicyBase {

  private string $emptyPolicyId = 'empty_policy';
  private string $emptyPolicyLabel = 'Empty policy';
  private string $emptyPolicyAddress = 'admin/structure/config_policy/empty_policy';

  protected function setUp(): void {
    parent::setUp();
    $empty_config_policy = $this->container->get('entity_type.manager')
      ->getStorage('config_policy')
      ->create([
        'id' => $this->emptyPolicyId,
        'label' => $this->emptyPolicyLabel,
        'description' => $this->policyDescription,
        'weight' => 0,
        'rules' => [],
      ]);
    $empty_config_policy->save();
    $this->container->get('router.builder')->rebuild();
  }

  public function testListConfigRule() {
    $this->drupalGet($this->policyAddress);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($this->ruleLabel);
  }

  public function testSelectableConfigRule() {
    $this->drupalGet($this->emptyPolicyAddress);
    $this->assertSession()->pageTextContains('Empty display mode');
  }

  public function testDeleteConfigRule() {
    $this->drupalGet($this->policyAddress);
    $this->clickLink('Delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('admin/structure/config_policy/' . $this->policyId . '/rules/' . $this->ruleId . '/delete');
    $this->assertSession()->pageTextContains('Are you sure you want to delete the');

    $this->submitForm([], 'Delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->policyAddress);

    $this->assertSession()->pageTextNotContains($this->ruleLabel);
  }

  public function testAddConfigRule() {
    $this->drupalGet($this->emptyPolicyAddress);
    $this->submitForm(['new' => 'empty_entity_view_display'], 'edit-add');
    $this->assertSession()->addressEquals($this->emptyPolicyAddress . '/rules/empty_entity_view_display/add');

    $rule_name = 'Test rule';
    $page = $this->getSession()->getPage();
    $page->fillField('edit-settings-label', $rule_name);
    $this->submitForm([], 'edit-submit');

    $this->assertSession()->addressEquals($this->emptyPolicyAddress);
    $this->assertSession()->pageTextContains($rule_name);
  }

}
