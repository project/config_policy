<?php

namespace Drupal\Tests\config_policy\Functional;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * @group Browser
 */
class UiConfigPolicyValidateTest extends UiConfigPolicyBase {

  private function createIssue() {
    $this->drupalCreateContentType([
      'type' => 'test_bundle',
      'name' => 'Test node type',
    ]);
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_test_content',
      'entity_type' => 'node',
      'type' => 'text_long',
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'test_bundle',
    ]);
    $field->save();
    $this->drupalCreateNode([
      'type' => 'test_bundle',
      'title' => 'Test title',
      'field_test_content' => 'long text',
    ]);
    $display_modes = $this->container->get('entity_type.manager')
      ->getStorage('entity_view_display')
      ->loadByProperties([
        'id' => 'node.test_bundle.default',
      ]);
    $display_mode = reset($display_modes);
    assert($display_mode instanceof EntityViewDisplayInterface);
    $display_mode->set('content', [
      'test_content' => [
        'settings' => [],
        'third_party_settings' => [],
        'weight' => 100,
        'region' => 'content',
      ],
    ]);
    $display_mode->save();
  }

  public function testConfirmValidate() {
    $this->drupalGet($this->policyAddress);
    $this->assertSession()->statusCodeEquals(200);
    $this->clickLink('Validate');
    $this->assertSession()->addressEquals($this->policyAddress . '/validate/' . $this->ruleId);
    $this->assertSession()->pageTextContains('You are about to validate');
    $this->submitForm([], 'edit-validate');
  }

  public function testValidateNoIssues() {
    $this->drupalGet($this->policyAddress);
    $this->clickLink('Validate');
    $this->submitForm([], 'edit-validate');
    $this->assertSession()->pageTextContains('No issues found.');
    $this->assertSession()->pageTextNotContains('No issues could be fixed.');
  }

  public function testFixNoIssues() {
    $this->drupalGet($this->policyAddress);
    $this->clickLink('Validate');
    $this->submitForm(['edit-fix' => 1], 'edit-validate');
    $this->assertSession()->pageTextContains('No issues found.');
    $this->assertSession()->pageTextContains('No issues could be fixed.');
  }

  public function testValidateIssues() {
    $this->createIssue();
    $this->drupalGet($this->policyAddress);
    $this->clickLink('Validate');
    $this->submitForm([], 'edit-validate');
    $this->assertSession()->pageTextNotContains('No issues found.');
    $this->assertSession()->pageTextNotContains('No issues could be fixed.');
    $this->assertSession()->pageTextContains('(1 invalid)');
    $this->assertSession()->pageTextContains('Nothing was fixed, as the fix option was not enabled.');
  }

  public function testFixIssues() {
    $this->createIssue();
    $this->drupalGet($this->policyAddress);
    $this->clickLink('Validate');
    $this->submitForm(['edit-fix' => 1], 'edit-validate');
    $this->assertSession()->pageTextNotContains('No issues found.');
    $this->assertSession()->pageTextNotContains('No issues could be fixed.');
    $this->assertSession()->pageTextContains('Fix results (1 fixed)');
  }

  public function testPrefixValidateIssues() {
    $this->createIssue();
    $this->drupalGet($this->policyAddress);
    $this->clickLink('Validate');
    $this->submitForm(['edit-prefix' => 'non-existing-prefix'], 'edit-validate');
    $this->assertSession()->pageTextContains('No issues found.');
    $this->assertSession()->pageTextNotContains('No issues could be fixed.');
    $this->assertSession()->pageTextContains('Nothing was fixed, as the fix option was not enabled.');
  }

  public function testPrefixFixIssues() {
    $this->createIssue();
    $this->drupalGet($this->policyAddress);
    $this->clickLink('Validate');
    $this->submitForm(['edit-fix' => 1, 'edit-prefix' => 'non-existing-prefix'], 'edit-validate');
    $this->assertSession()->pageTextContains('No issues found.');
    $this->assertSession()->pageTextContains('No issues could be fixed.');
  }

}
