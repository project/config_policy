<?php

namespace Drupal\Tests\config_policy\Functional;

use Drupal\Tests\BrowserTestBase;

abstract class UiConfigPolicyBase extends BrowserTestBase {

  protected static $modules = ['config_policy', 'field', 'node'];
  protected $defaultTheme = 'claro';
  protected string $policyId = 'test_policy';
  protected string $policyLabel = 'Test policy';
  protected string $ruleId = '496d7909-779b-4668-89de-64c588be24f2';
  protected string $ruleLabel = 'Test rule';
  protected string $listAddress = 'admin/structure/config_policy';
  protected string $policyDescription = 'My description';
  protected string $policyAddress = 'admin/structure/config_policy/test_policy';

  protected function setUp(): void {
    parent::setUp();
    $config_policy = $this->container->get('entity_type.manager')
      ->getStorage('config_policy')
      ->create([
        'id' => $this->policyId,
        'label' => $this->policyLabel,
        'description' => $this->policyDescription,
        'status' => 1,
        'weight' => 0,
        'rules' => [
          $this->ruleId => [
            'uuid' => $this->ruleId,
            'id' => 'empty_entity_view_display',
            'weight' => 1,
            'settings' => [
              'label' => $this->ruleLabel,
              'entity_types' => ['node'],
              'bundles' => ['test_bundle'],
              'view_modes' => ['node.default'],
              'validate_runtime' => 1,
              'fix_runtime' => 0,
              'prevent' => 0,
              'validate_import' => 0,
            ],
          ],
        ],
      ]);
    $config_policy->save();
    $this->container->get('router.builder')->rebuild();

    $account = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($account);
  }

}
