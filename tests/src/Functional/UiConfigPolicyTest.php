<?php

namespace Drupal\Tests\config_policy\Functional;

/**
 * @group Browser
 */
class UiConfigPolicyTest extends UiConfigPolicyBase {

  public function testListConfigPolicy() {
    $this->drupalGet($this->listAddress);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($this->policyLabel);
    $this->assertSession()->pageTextContains($this->policyDescription);
  }

  public function testAddConfigPolicy() {
    $this->drupalGet('admin/structure/config_policy/add');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Add config policy to a field');

    $this->submitForm([
      'id' => 'new_policy',
      'label' => 'New policy',
      'description' => 'New description',
    ], 'Save');

    $this->drupalGet($this->listAddress);
    $this->assertSession()->pageTextContains('New policy');
    $this->assertSession()->pageTextContains('New description');
  }

  public function testDeleteFieldUsagePolicy() {
    $this->drupalGet($this->listAddress);
    $this->assertSession()->statusCodeEquals(200);

    $this->clickLink('Delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('admin/structure/config_policy/' . $this->policyId . '/delete');
    $this->assertSession()->pageTextContains('Are you sure you want to delete the ' . $this->policyLabel . ' policy?');

    $this->submitForm([], 'Delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals($this->listAddress);

    $policy_count = count($this->container->get('entity_type.manager')
      ->getStorage('config_policy')
      ->loadMultiple());
    $this->assertEquals(0, $policy_count, 'Expected 0 config policies after deleting.');

    $this->drupalGet($this->listAddress);
    $this->assertSession()->pageTextNotContains($this->policyLabel);
  }

}
